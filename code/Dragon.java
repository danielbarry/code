import java.io.Serializable;

/**
 * The Dragon class stores the information relative to a Dragon, including whether
 * the Dragon is capable of talking. If possible, the Dragon attempts to load
 * configuration settings relating to a hire fee and skill level. This is done so
 * that in future upgrades the game can be modified from a configuration file. If
 * this file fails to load, the default constants are refered to instead.
 * <br />
 * This class also offers simple methods for accessing and mutating data
 * accordingly with very simple processing. The data it contains is also
 * serializable meaning that an instance can be saved to file.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 21:45 23/02/2014 Prototype
 */
public class Dragon extends Champion implements Serializable{
    /* A set serial number so that when a serialized object is reloaded it reloads
     * with the same UID, even if the JVM is reset. */
    private static final long serialVersionUID = 32L;
    /* The location to attempt to load the default configuration file. */
    private static final String CONSTANT_FILE = "champion_dragon_defaults.config";
    /* Default values to be reffered to on failure to load other values from the
     * configuration file. */
    private static final int SKILL_LEVEL_DEF = 7;
    private static final int HIRE_AMOUNT_DEF = 500;
    /* The default values used by this class to define constant values throughout
     * the class. */
    private static int SKILL_LEVEL;
    private static int HIRE_AMOUNT;
    
    /* Run once initialiser to potentially load in the properties from file. If
     * the values are not found for any reason the default ones specified by the
     * documentation are used. */
    static{
        Property prop = new Property();
        prop.loadFile(CONSTANT_FILE);
        SKILL_LEVEL = prop.loadValue("SKILL_LEVEL", SKILL_LEVEL_DEF, Integer.class);
        HIRE_AMOUNT = prop.loadValue("HIRE_AMOUNT", HIRE_AMOUNT_DEF, Integer.class);
    }
    
    /* Values that define this Champion from other types of Champion. */
    private boolean canTalk;
    
    /**
     * This constructor allows an instance of Dragon to be created. The details
     * that define a Dragon are it's name and ability to talk.
     * <br />
     * An attempt is made to laod constant from a properties file if it is
     * possible. These values are automatically casted and do not need to be
     * worried about. If the value if not loaded for any reason, the defaults value
     * is returned instead.
     * 
     * @param name The name that defines the Champion. This must be unique in
     * respect to all Champions. Failing to meet this pre-condition will result in
     * a custom NameExistsException as defined in the Champion class.
     * @param canTalk This defines whether the Dragon is able to talk. This is
     * stored as a boolean value, where True represents the Drgon's ability to talk
     * whereas False means the Dragon is not able to talk.
     * @throws NameExistsException This exception is thrown if the name of the
     * Champion has already been instantiated and currently exists.
     * @throws SkillLevelException This exception is thrown if the skill level of a
     * Champion is less than or greater than the minimum and maxmium accepted
     * levels respectively.
     */
    public Dragon(String name, boolean canTalk) throws Champion.NameExistsException, SkillLevelException{
        super(name, SKILL_LEVEL, HIRE_AMOUNT);
        this.canTalk = canTalk;
    }
    
    /**
     * This allows a Dragon to return whether it is able to talk or not. This value
     * is important for determining if the Dragon in question is able to compete in
     * a fight due to Challenge type.
     * 
     * @return The Dragon's ability to talk, true being yes and false being no.
     */
    public boolean canTalk(){ return canTalk; }
    
    /**
     * Returns a message containing the basic information about a Champion,
     * including it's type (which is a String value of child class name), the
     * skill level of the Champion, the amount to hire the Champion and the
     * state the Champion is currently in.
     * 
     * @return A message containing the basic information about the Champion.
     */
    @Override
    public String toString(){
        return super.toString()
           + "\nCan Talk   : " + canTalk;
    }
}
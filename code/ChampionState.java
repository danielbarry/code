/* Addition: Changed the import java.io.* to java.io.Serializable so that
 * programmers are aware of any additional classes they use, and therefore the
 * potential overhead of doing so. This may have future impact on RAM but in
 * general is good practice to be aware of what is being imported into a
 * project. */
import java.io.Serializable;

/**
 * Enumeration class ChampionState 
 * 
 * @author Daniel Barry & Paul Moggridge
 * @version 1.1 21:10 03/03/2014 Prototype
 */
public enum ChampionState implements Serializable{
    /* Addition: Removed the space before the return type as the return String
     * should NOT assume anything about how it will be used. It is also wasteful
     * memory. */
    FORHIRE("For hire"), ACTIVE("Active"), RESTING("Resting"), DEAD("Dead");
    
    private String state;
    
    private ChampionState(String st){ state = st; }
    
    /* The @Override annotation has also been added to tell future programmers
     * that overriding the toString() method was intentional. */
    @Override
    public String toString(){ return state; }
}

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.util.ArrayList;

/**
 * This class is responsible for handling the front end of the Game, allowing the
 * user to play a game through an implementation of Game. The error handling of
 * this Class is soley on user input, where all other error handling should be
 * done by the implenting Class of Game.
 * <br />
 * This Class currently instantiates a new Game with a player named "Hero" which
 * is hard-coded. This then builds a game using the hard-coded values in the
 * Player class.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 03:55 01/03/2014 Prototype
 */
public class GameGUI implements ActionListener{
    /**
     * This Enumerator allows for interface styles to be defined. This is useful
     * if there are more than one states in which soemthing may be in and you want
     * to garuntee a state. This both reduces code, and simplifies the state of
     * the interface to a simple enumerator. In the future there may be more than
     * one view mode and it will be important to make sure that these flow
     * correctly. It may be also important to know what state the interface is in
     * to make descisions about it's future, allowing for a simple sub-sumption
     * architecture.
     * <br />
     * Current states are:
     * <ul>
     *   <li>Text Area - Displays the text area and a button allowing the text
     *   area to be cleared.</li>
     *   <li>No Text Area - Makes the text are invisible and removes the button
     *   allowing the text are to be cleared.</li>
     * </ul>
     */
    private enum ViewMode{ TEXT_AREA, NO_TEXT_AREA;}
    
    private Game player;
    private JFrame frame;
    private JPanel panel, buttons;
    private JMenuBar menuBar;
    private JTextArea textArea;
    /* An ArrayList containing all of the GUI objects, so that they may be
     * dynamically checked to see what part was clicked or to refer them quickly
     * by index. It is not strictly needed but does make deciding on what to do
     * upon an event very trivial. */
    private ArrayList<AbstractButton> guiObjects = new ArrayList<AbstractButton>();
    private ViewMode mode = ViewMode.NO_TEXT_AREA;
    
    /**
     * The Game GUI's point of entry. Although a String array of arguments is
     * taken from the terminal, currently none of them are used. All values in
     * this program are hard-coded as per the specimen.
     * 
     * @param args Arguments from the Terminal that are currently ignored.
     */
    public static void main(String[] args){ new GameGUI(); }
    
    /**
     * The sole constructor of the GameGUI class. This takes a player's name
     * and creates a new game using this information. The Player's name will go
     * on to determine the title bar and the Ruler's name in the instance of
     * the Game.
     */
    public GameGUI(){
        String player = getString("Player Name", "Hero");
        this.player = new Player(player);
        initFrame(player);
        initPanel();
        initMenuBar();
        initContent();
        setupView();
        frame.setVisible(true);
    }
    
    /**
     * A simple initialization of the frame with a given name.
     * 
     * @param name The name of the Window to be used as a title for the Window.
     */
    private void initFrame(String name){
        frame = new JFrame("CODE - " + name);
        frame.setIconImage(new ImageIcon("res/codeIcon.png").getImage());
        frame.setSize(700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * A simple initialization of the panels layout.
     */
    private void initPanel(){
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        frame.add(panel);
        buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 4));
        panel.add(buttons, BorderLayout.NORTH);
    }
    
    /**
     * A simple initialization of the frame's menu bar.
     */
    private void initMenuBar(){
        menuBar = new JMenuBar();
        String[] items = new String[]{"List all Champions", "List my Army", "Hire Champion", "Dismiss Champion", "Restore Champion", "View Champion"};
        addMenu("Champions", items);
        frame.setJMenuBar(menuBar);
    }
    
    /**
     * A simple initialization of the button content to the top section of the
     * frame. The text area is also added as the center peice of the frame.
     */
    private void initContent(){
        addButton("Meet Challenge", "res/meetchall.png");
        addButton("View State", "res/view.png");
        addButton("Clear", "res/clear.png");
        addButton("Quit", "res/exit.png");
        addTextArea();
    }
    
    /**
     * A simple initialization of a menu and all of the options under it. This
     * is not capable of having multiple sub menus and only acts as a simpler
     * way of adding 2d menus.
     * 
     * @param menuName The name to be displayed as the name of the menu. When
     * clicked the options it contaisn are displayed.
     * @param items The options to be displayed under the menu name once
     * clicked. Each one of these options is added to the event listener so
     * that clicks are heard and can be acted upon.
     */
    private void addMenu(String menuName, String[] items){
        JMenu menu = new JMenu(menuName);
        for(int i=0; i<items.length; i++){
            JMenuItem menuItem = new JMenuItem(items[i]);
            menuItem.addActionListener(this);
            menu.add(menuItem);
            guiObjects.add(menuItem);
        }
        menuBar.add(menu);
    }
    
    /**
     * A simple initialization of a single button with a given displayed text,
     * where it is added both to the event listener and to the guiObjects array
     * allowing it's index to be switched on at a later date.
     * 
     * @param buttonText The text to be displayed on the button.
     */
    private void addButton(String buttonText, String iconPath){
        JButton button = new JButton(buttonText, new ImageIcon(iconPath));
        button.addActionListener(this);
        buttons.add(button);
        guiObjects.add(button);
    }
    
    /**
     * Add a text area to the center of the screen. Please note that there is
     * currently only one but this may at some point be extended to contain
     * multiple text boxes for any number of reasons. The text area is set up to
     * scroll as appropriate and to not be editable, as the user interacts with
     * the other parts of the GUI and not the text area. The text area is also
     * set to wrap words to reduce the amount of scrolling horizontally needed.
     */
    private void addTextArea(){
        textArea = new JTextArea("", 5, 20);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);
        JScrollPane scroll = new JScrollPane(textArea);
        panel.add(scroll, BorderLayout.CENTER);
    }
    
    /**
     * The overriden action listener method to catch events. All objects that
     * interact with user button click input is handled here. As many of the
     * required actions are the same and some even do exactly the same task,
     * the actions are done within an enumerator to reduce duplication. This is
     * reasonably efficient and very easily maintainable.
     * 
     * @param ae The event that occured to trigger this method being called.
     */
    @Override
    public void actionPerformed(ActionEvent ae){
        AbstractButton comp = (AbstractButton)(ae.getSource());
        int item = guiObjects.indexOf(comp);
        textArea.append("\n\n" + comp.getText() + "\n");
        switch(item){
            /* List all Champions */
            case 0  : textArea.append(player.getAllChampionsForHire());
                break;
            /* List my Army */
            case 1  : textArea.append(player.getArmy());
            /* Hire Champion */
            case 2  : textArea.append(player.hireChampion(getChampionName(comp.getText())));
                break;
            /* Dismiss Champion */
            case 3  : textArea.append(player.dismissChampion(getChampionName(comp.getText())) ? "Champion dismissed" : "Champion not dismissed");
                break;
            /* Restore Champion */
            case 4  : textArea.append(player.restoreChampion(getChampionName(comp.getText())) ? "Champion restored" : "Champion not restored");
                break;
            /* View Champion */
            case 5  : textArea.append(player.getChampion(getChampionName(comp.getText())));
                break;
            /* Meet Challenge */
            case 6  : textArea.append(player.meetChallenge(getChallengeNumber(comp.getText())));
                      if(player.hasLost())exit();
                break;
            /* View State */
            case 7  : textArea.append(player.toString());
                break;
            /* Clear */
            case 8  : textArea.setText("");
                break;
            /* Quit */
            case 9  : exit();
                break;
            /* Default action, where if a button is not found a information
             * dialod box is displayed telling the user the feature is not
             * yet implemented. */
            default : informationPane("Feature not implemented.");
        }
        mode = item<=7 ? ViewMode.TEXT_AREA : ViewMode.NO_TEXT_AREA;
        setupView();
    }
    
    /**
     * Exits the Gui while displaying an information dialog box containing a
     * message about the Player's current state.
     */
    private void exit(){
        informationPane(player.toString());
        System.exit(0);
    }
    
    /**
     * Allows for the view to be changed depending on what the enumerator is
     * set to. This method also allows for a refresh of the current layout of
     * the GUI to be done at any time if called upon.
     */
    private void setupView(){
        /* Auto-scroll the Text Area to the most recent ammendments (the
         * bottom of the Text Area). */
        textArea.setCaretPosition(textArea.getDocument().getLength());
        switch(mode){
            case TEXT_AREA      : textArea.setVisible(true);
                                  guiObjects.get(8).setVisible(true);
                break;
            case NO_TEXT_AREA   : 
            default             : textArea.setVisible(false);
                                  guiObjects.get(8).setVisible(false);
        }
    }
    
    /**
     * This method clears the text area. In the future it may want to be done
     * another way and has therefore been branched off to do so. It's very
     * rare that a user of any application of any kind will want to not be
     * able to recall some sort of history of previous actions/commands.
     */
    private void clearTextArea(){ textArea.setText(""); }
    
    /**
     * Returns a Champion name as typed in by the user. If the user fails to
     * type in the Champion name a empty String can be returned instead of
     * null (which could potentially crash the system).
     * 
     * @param msg The message to be displayed to the user to prompt for the
     * correct input response. This is displayed with the option pane when
     * the user is inputing data.
     * @return The requested data type in the correct format, not containign
     * anything that should be able to crash the system.
     */
    private String getChampionName(String msg){
        return getString("\nChampion Name:", "");
    }
    
    /**
     * Returns a Challenge number represented as an Integer to the user. If
     * the user fails to enter a valid Integer into the system, a value of 
     * '-1' is returned as a safe and non-destructive user input instead.
     * 
     * @param msg The message to be displayed to the user to prompt for the
     * correct input response. This is displayed with the option pane when
     * the user is inputing data.
     * @return The requested data type in the correct format, not containing
     * anything that should be able to crash the system.
     */
    private int getChallengeNumber(String msg){
        try{ return Integer.parseInt(getString(msg + "Champion Name:", "")); }
        catch(NumberFormatException e){ return -1; }
    }
    
    /**
     * Returns a String value from the user as typed, with the message used
     * as a prompt for input type. If no value is entered, a blnk String is
     * returned.
     * 
     * @param msg The message to be displayed to the user to prompt for the
     * correct input response. This is displayed with the option pane when
     * the user is inputing data.
     * @param def The value to come pre-entered into the input box on the
     * Option Pane.
     * @return The requested data type in the correct format, not containing
     * anything that should be able to crash the system.
     */
    private String getString(String msg, String def){
        String result = JOptionPane.showInputDialog(frame, msg, def);
        return result!=null ? result : "";
    }
    
    /**
     * Makes an information dialog box display, where informaton relative to
     * the user's current actions are displayed and control of the current
     * frame is lost until the dialog is dealt with. The thread that this is
     * run from is also halted, something that should be considered when
     * implementing.
     * 
     * @param msg The message to be displayed to the user informating them of
     * relevant information to their current actions.
     */
    private void informationPane(String msg){ JOptionPane.showMessageDialog(frame, msg); }
}
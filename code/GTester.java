import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.GridLayout;

/**
 * Comprehensive code for all code adjusted by Daniel Barry
 * & Paul Moggridge. Where possible, the Test Plan written
 * in the earlier assignment (Part 1) is used. This Tester
 * should be run before release and during programming to
 * ensure code quality and where possible prevent
 * regression.
 * 
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 prototype
 */
public class GTester{
    private JFrame resultsWindow = new JFrame("GTester - Results");
    private Game plyr = null;
    
    public static void main(String[] args){ GTester gt = new GTester(); }

    public GTester(){
        resultsWindow.setSize(800,600);
        resultsWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        resultsWindow.setLayout(new GridLayout(7,4));
        resultsWindow.setVisible(true);
        plyr = new Player("Horatio");
        runTests();
    }
    
    public void runTests(){
        printTest("Dismiss a Champion, with a non exixstent champion.",0);
        printTest("Get a Champion, with a non exixstent champion.",1);
        printTest("Hire a Champion, with a non exixstent champion.",2);
        printTest("List all in army, when there's no champions in it.",4);
        printTest("List for Hire, Sunny day.",5);
        printTest("Selecting a Challenege, with an invalid challenege number.",6);
        printTest("Restore a Champion, with an invalid champion name.",7);
        printTest("Getting a Champion, Sunny day scenario.",8);
        printTest("Getting Ruler Name, Sunny dat scenario.",9);
        printTest("Getting the Treasury Value, Sunny day scenario.",10);
        printTest("Hiring a Champion, Sunny day scenario.",11);
        printTest("Restore a Champion, that is already active.",12);
        printTest("List army, sunny day scenario.",14);
        printTest("List all for hire, Sunnay day scenario.",15);       
        printTest("Hire a Champion, that's already hired.",16);
        printTest("Meeting a Challenge, Sunny day scenario.",17);
        printTest("Meet a Challenege, with a only a resting Champion.",18);
        printTest("Restore a Champion, Sunny day scenario.",19);
        printTest("Meeting a Challenge, with a champion of wrong type.",20);
        printTest("Meeting a Challenge, with a champion with a too low skill level.",21);
        printTest("Meeting a Challenege, with a dead champion.",22);
        printTest("Restore a Champion, with a dead champion.",23);
        printTest("Restore a Champion, that is un-hired.",24);
        printTest("Hiring a Champion, with in-susficient funds.",25);
        printTest("Dismiss a Champion, a dead champion.",26);
        printTest("Dismiss a Champion, sunny day scenario.",27);
    }

    public void printTest(String desc , int testIndex){        
        //Tile for this test
        JPanel testBox = new JPanel();
        testBox.setLayout(new GridLayout(2,1));
        testBox.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        testBox.add(new JLabel(desc));
        //Panel for lower section
        JPanel statusArea = new JPanel();
        statusArea.setLayout(new GridLayout(1,2));
        String status = "running test...";
        JLabel statusLabel = new JLabel(status);
        statusArea.add(statusLabel);
        JLabel statusPic = new JLabel(new ImageIcon("res/loadIcon.png"));
        resultsWindow.repaint();
        statusArea.add(statusPic);
        testBox.add(statusArea);
        resultsWindow.add(testBox);
        delay();
        resultsWindow.repaint();
        if(doTest(testIndex)){
            statusArea.remove(statusPic);
            statusPic = new JLabel(new ImageIcon("res/passedIcon.png"));
            statusArea.add(statusPic);
            status = "Test Complete : Success";
            statusLabel.setText(status);
            testBox.setBorder(BorderFactory.createLineBorder(Color.GREEN));
        }else{
            statusArea.remove(statusPic);
            statusPic = new JLabel(new ImageIcon("res/failedIcon.png"));
            statusArea.add(statusPic);
            status = "Test Complete : Failed";
            statusLabel.setText(status);
            testBox.setBorder(BorderFactory.createLineBorder(Color.RED));
        }        
        resultsWindow.repaint();       
    }
   
    public boolean doTest(int testIndex){
        switch(testIndex){
            case 0:  return !plyr.dismissChampion("Soap") && 1000==plyr.getMoney();
            case 1:  return null == plyr.getChampion("Soap") && 1000==plyr.getMoney();
            case 2:  return plyr.hireChampion("Soap").contains("- not found") && 1000==plyr.getMoney();
            case 4:  return plyr.getArmy().equals("") && 1000==plyr.getMoney();
            case 5:  return plyr.getAllChampionsForHire().contains("Name") && 1000==plyr.getMoney();
            case 6:  return null == plyr.getChallenge(400) && 1000==plyr.getMoney();
            case 7:  return !plyr.restoreChampion("Paulius") && 1000==plyr.getMoney();
            case 8:  return plyr.getChampion("Golum").contains("Golum") && 1000==plyr.getMoney();
            case 9:  return plyr.getName().equals("Horatio") && 1000==plyr.getMoney();
            case 10: return 1000 == plyr.getMoney();
            case 11: return plyr.hireChampion("Flimsi").equals("Flimsi- hired and available") && 700==plyr.getMoney();
            case 12: return !plyr.restoreChampion("Flimsi") && 700==plyr.getMoney();
            case 14: return plyr.getArmy().contains("Flimsi") && 700==plyr.getMoney();
            case 15: return plyr.getAllChampionsForHire().contains("Name       : Drabina") && 700==plyr.getMoney();
            case 16: return plyr.hireChampion("Flimsi").contains("- cannot be hired") && 700==plyr.getMoney();
            case 17: return plyr.meetChallenge(2).contains("Challenge won by") && 820==plyr.getMoney();
            case 18: return plyr.meetChallenge(2).contains("Challenge lost as no champion available") && 700==plyr.getMoney();
            case 19: return plyr.restoreChampion("Flimsi") && 650==plyr.getMoney();
            case 20: return plyr.meetChallenge(1).equals("Challenge lost as no champion available") && 550==plyr.getMoney();
            case 21: return plyr.meetChallenge(5).contains("Challenge lost on skill level.") && 460==plyr.getMoney();
            case 22: return plyr.meetChallenge(2).contains("Challenge lost as no champion available") && 340==plyr.getMoney();
            case 23: return !plyr.restoreChampion("Flimsi") && 340==plyr.getMoney();
            case 24: return !plyr.restoreChampion("Argon") && 340==plyr.getMoney();
            case 25: return plyr.hireChampion("Xenon").contains("- not enough money") && 340==plyr.getMoney();
            case 26: return !plyr.dismissChampion("Flimsi") && 340==plyr.getMoney();
            case 27: plyr.hireChampion("Elblond");
                     return plyr.dismissChampion("Elblond") && 240==plyr.getMoney();
            default: System.err.println("Test not found");
                     return false;
        }
    }
   
    public void delay(){
        try{ Thread.sleep(70); }
        catch(Exception e){}
    }
}
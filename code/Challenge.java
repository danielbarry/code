import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * The Challenge class stores and provides access to information in repsects to
 * a challenge. The following information is stored:
 * <ul>
 *   <li>Challenge Number - The unique number given to a Challenge. This is
 *   checked for uniqueness in the construction of an object.</li>
 *   <li>Skill Level - This Challenge's skill level, which is used to determine
 *   whether a Champion wins or loses a Challenge.</li>
 *   <li>Challenge Type - This dictates whether a Champion is able to
 *   participate in a Challenge.</li>
 *   <li>Reward - The reward amount for winning this challenge to be added to
 *   the treasury.</li>
 *   <li>Enemy - A String containing the name of the enemy. This doesn't have
 *   any effect on gameplay.</li>
 * </ul>
 * 
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 20:20 24/02/2014 Prototype
 */
public class Challenge implements Serializable{
    /* A set serial number so that when a serialized object is reloaded it reloads
     * with the same UID, even if the JVM is reset. */
    private static final long serialVersionUID = 2L;
    /* An ArrayList that keeps a weak refference to Challenge instances, in which
     * can be used to check whether a Challenge created has a true unique
     * identifier number as per the specification. */
    private static ArrayList<WeakReference<Challenge>> instances = new ArrayList<WeakReference<Challenge>>();
    /* The location to attempt to load the default challenge configuration. */
    private static final String CONSTANT_FILE = "challenge_defaults.config";
    /* The values to default to if values from the configuration can't be
     * loaded. */
    private static final int MIN_SKILL_LEVEL_DEF = 1, MAX_SKILL_LEVEL_DEF = 10;
    /* The values to be used to check the skill level boundaries that a Challenge is
     * expected to be able to have. */
    private static int MIN_SKILL_LEVEL, MAX_SKILL_LEVEL;
    
    /* Initializer setup to load the values of MIN_SKILL_LEVEL and MAX_SKILL_LEVEL
     * from a file if possible. Any excpetions that occur from this have been
     * declared as caught and are returned to their default values defined by
     * MIN_SKILL_LEVEL_DEF and MAX_SKILL_LEVEL_DEF. */
    static{
        /* Attempt to load the default properties file with the values. On failure,
         * just use the ones specified. */
        Property prop = new Property();
        prop.loadFile(CONSTANT_FILE);
        MIN_SKILL_LEVEL = prop.loadValue("MIN_SKILL_LEVEL", MIN_SKILL_LEVEL_DEF, Integer.class);
        MAX_SKILL_LEVEL = prop.loadValue("MAX_SKILL_LEVEL", MAX_SKILL_LEVEL_DEF, Integer.class);
    }
    
    /* Store the unique identifier number of a Challenge and the skill level that
     * will indicate a win or lose in Challenge by a Champion. */
    private int challengeNo, skillLevel;
    /* The type of a Challenge, which is useful for knowing whether a Champion is
     * able to fight. */
    private ChallengeType type;
    /* The reward amount for winning this Challenge. */
    private int reward;
    /* The enemy the Champion will be fighting. */
    private String enemy;
    
    /**
     * This has been created to allow programmers using the Challenge class to get
     * feedback on Challenge creation. This particular Exception is thrown if a
     * Challenge number already exists, as this breaks the rule of a Challenge having
     * a unique identifier as it's number.
     */
    public class ChallengeExistsException extends Exception{
        /**
         * This constructor handles the details of the Exception with a message
         * containing detials about the particular instance.
         * 
         * @param s The message to be printed explaining the Exception thrown.
         */
        public ChallengeExistsException(String s){ super(s); }
    }
    
    /**
     * Sole Constructor. This constructor is invoked directly. The information that
     * decribes a Challenge is passed in order to store details about a Challenge.
     * <br />
     * Challenges are currently checked to make sure that the challenge number is a
     * unique identifier. A non-unique challenge number will throw a
     * ChallengeExistsException.
     * 
     * @param challengeNo The number a Challenge which is alos a unique identifier.
     * @param type The type of this Challenge. This will vary and will change
     * whether a Champion is able to fight in a Challenge. The following types of
     * Challenge exist:
     * <ul>
     *   <li>Magic</li>
     *   <li>Fight</li>
     *   <li>Mystery</li>
     * </ul>
     * @param reward This is the amount a Player recieves upon a successful fight.
     * @param skillLevel The skill level determines whether a Champion is capable
     * of fighting in this Challenge. If the skill level is outside of the accepted
     * range, an custom SkillLevelException is thrown.
     * @param enemy Specifies the enemy the Champion will be fighting.
     * @throws ChallengeExistsException This excpetion is thrown if the Challenge
     * number is not a unique identfier and a Challenge with this number has a
     * strong reference to it somewhere within the local JVM.
     * @throws SkillLevelException This exception is thrown if the skill level of a
     * Challenge is less than or greater than the minimum and maxmium accepted
     * levels respectively.
     */
    public Challenge(int challengeNo, ChallengeType type, int reward, int skillLevel, String enemy) throws ChallengeExistsException, SkillLevelException{
        /* Save the value of the passed number to this Challenge */
        this.challengeNo = challengeNo;
        /* Check this Challenge number to find out whether it is a unique
         * identifier. Whilst checking, it is important that if we come across a
         * weak reference that it is removed from the ArrayList to save space and
         * future time. */
        for(WeakReference<Challenge> challenge : instances){
            Challenge chall = challenge.get();
            if(chall!=null){
                /* Make sure that the refference isn't weak before attempting
                 * check. */
                if(chall.challengeNo==this.challengeNo){
                    /* Output a disruptive error message to the programmer for using the
                     * Challenge incorrectly. */
                    throw new ChallengeExistsException("Unique identifier of Challenge already exists.");
                }
            }else{
                /* If reference is weak, remove it from the ArrayList so we don't
                 * waste time or space with it again. */
                instances.remove(chall);
            }
        }
        /* Add this instance to a collection of all instances. Keep reference to
         * whole instance as it may prove useful later and doesn't add noticeable
         * overhead. The reason for using a Weak Reference in this case is to
         * reduce coupling throughout the whole program as per the
         * specification. */
        instances.add(new WeakReference<Challenge>(this));
        /* Store the rest of the information passed. */
        this.type = type;
        this.reward = reward;
        this.skillLevel = skillLevel;
        if(this.skillLevel<MIN_SKILL_LEVEL||this.skillLevel>MAX_SKILL_LEVEL){
            /* Output a disruptive error message to the programmer for using the
             * Challenge incorrectly. */
            throw new SkillLevelException("Skill level is not allowed. Must be between " + MIN_SKILL_LEVEL + " and " + MAX_SKILL_LEVEL + ".");
        }
        this.enemy = enemy;
    }
    
    /**
     * This returns the Challenge number of a challenge, as this may be needed by
     * a Class for identifying a unique Challenge.
     * 
     * @return The Challenge number associated with this Challenge.
     */
    public int getChallengeNo(){ return challengeNo; }
    
    /**
     * This returns the enemy that the Challenge contains.
     * 
     * @return The information relative to the enemy in this particular
     * Challenge.
     */
    public String getEnemy(){ return enemy; }
    
    /**
     * The reward amount relative to the Challenge.
     * 
     * @return The reward amount available for winning a challenge.
     */
    public int getRewardAmount(){ return reward; }
    
    /**
     * Returns the result of a boolean expression of if this Challenge is of
     * type Magic.
     * 
     * @return Whether this Challenge is of type Magic.
     */
    public boolean isMagic(){ return type==ChallengeType.MAGIC; }
    
    /**
     * Returns the result of a boolean expression of if this Challenge is of
     * type Fight.
     * 
     * @return Whether this Challenge is of type Fight.
     */
    public boolean isFight(){ return type==ChallengeType.FIGHT; }
    
    /**
     * Returns the result of a boolean expression of if this Challenge is of
     * type Mystery.
     * 
     * @return Whether this Challenge is of type Mystery.
     */
    public boolean isMystery(){ return type==ChallengeType.MYSTERY; }
    
    /**
     * Returns the skill level of the Challenge which is a value that resides
     * between the expected minimum and maximum values.
     * 
     * @return The skill level of this Challenge, used when determining whether
     * a fight is one or lost of skill level.
     */
    public int getSkillLevel(){ return skillLevel; }
    
    /**
     * Teturns the type of Challenge contained in the particular instance. This
     * is used to determine whether a challenge can be met.
     * 
     * @return The type of Challenege, which is a member of ChallengeType
     * enumerators.
     */
    public ChallengeType getType(){ return type; }
    
    /**
     * Retuns a String containing information relative to the Challenge
     * including the following:
     * <ul>
     *   <li>Challenge</li>
     *   <li>Type</li>
     *   <li>Reward</li>
     *   <li>Skill Level</li>
     *   <li>Enemy</li>
     * </ul>
     * 
     * @return Information about the Challenge.
     */
    @Override
    public String toString(){
        return "\nChallenge  : " + challengeNo
           + "\nType       : " + type.toString()
           + "\nReward     : " + reward
           + "\nSkill Level: " + skillLevel
           + "\nEnemy      : " + enemy;
    }
}
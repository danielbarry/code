import java.lang.reflect.Field;

/**
 * Comprehensive code for all code adjusted by Daniel Barry
 * & Paul Moggridge. Where possible, the Test Plan written
 * in the earlier assignment (Part 1) is used. This Tester
 * should be run before release and during programming to
 * ensure code quality and where possible prevent
 * regression.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 Prototype
 */
public class Tester{
    private static final int MAX_WIDTH = 79;
    
    private boolean time = true;
    private Game plyr = null;
    
    public static void main(String[] args){
        if(args.length>0){
            if(args[0].toLowerCase().equals("-t"))new Tester(false);
            else new Tester(true);
        }else new Tester(true);
    }
    
    public Tester(boolean time){
        this.time = time;
        System.out.print("\f");
        
        /* Self Test */
        System.out.println(createTitle('#', MAX_WIDTH, "Self Test"));
        String testing = "Tests ability to throw Assertion Errors upon a false assertion";
        test(true, "Assertion true", testing, "Test passes correctly.");
        try{ test(false, "Assertion false", testing, "Test doesn't pass correctly and throws an Assertion Error."); }
        catch(AssertionError e){ System.out.println(centerString(">>>> Above test passed. <<<<", MAX_WIDTH)); }
        
        /* Setup of Test Data */
        System.out.println(createTitle('@', MAX_WIDTH, "Setup of Test Data"));
        setup();
        
        /* Start of Tests */
        System.out.println(createTitle('#', MAX_WIDTH, "Start of Tests"));
        
        /* 6.1 Constructor(rulerName : String) */
        System.out.println(createTitle('=', MAX_WIDTH, "6.1 Constructor(rulerName : String)"));
        testing = "The ability to correctly create and implement the interface CODE.";
        test(plyr instanceof Player, "Creation of implementing class", testing, "A new Player is created for the game. Implementing class is expected to request a String containing the user's name.");
        test(plyr instanceof Game, "CODE methods exist", testing, "Player may not be a implementing instance of the interface CODE and should therefore be checked to make sure that the methods have been implemented before testing any further.");
        
        /* 6.2 getName(): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.2 getName(): String"));
        testing = "The ability to retrieve ruler name. This may be used by the UserIO and should therefore be checked.";
        test(plyr.getName().equals("Horatio"), "Retrieve ruler name", testing, "UserIO wants to print the ruler�s name in a message displayed by an opposing player.");
        
        /* 6.3 restoreGame(filename : String): Game */
        System.out.println(createTitle('=', MAX_WIDTH, "6.3 restoreGame(filename : String): Game"));
        testing = "The game�s ability to load Champions and Challenges from disk.";
        test(plyr.restoreGame("test/notHere.dat")==null, "A non existing file", testing, "A file that doesn�t exist is attempted to be loaded.");
        test(plyr.restoreGame("test/olenka.weird")==null, "A file with the wrong extension", testing, "A file name with the wrong extension will be attempted to be loaded.");
        test(plyr.restoreGame("test/blank.dat")==null, "A file with a correct name but blank", testing, "A blank file that exists will not be loaded as not correctly formatted.");
        test(plyr.restoreGame("test/corrupt.dat")==null, "A file with corrupted contents", testing, "A file is loaded that exists that has corrupted contents.");
        test(plyr.restoreGame("test/olenka.txt")!=null, "A correctly formatted file with test data", testing, "A file is loaded that exists that has correctly formatted data.");
        
        /* 6.4 getChampion(name : String) : String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.4 getChampion(name : String) : String"));
        testing = "The methods ability to correctly return champion details when data is required to be returned, otherwise a message indicating the circumstances as to why the champion data could not be returned.";
        test(plyr.getChampion("Ganfrank")!=null, "Correctly return a valid champion", testing, "Player wants to retrieve information about a champion to figure out if they are worth hiring.");
        test(plyr.getChampion("Bob Marley")==null, "Champion doesn't exist", testing, "Player wants to retrieve information about a player that doesn't exist. This may be because of a misspelling or simply the player is testing the boundaries of the system.");
        
        /* 6.5 getAllChampions(): String[] */
        //OMMITED DUE TO NOT BEING A METHOD IN THE CODE INTERFACE//
        
        /* 6.6 hireChampion(name : String): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.6 hireChampion(name : String): String"));
        testing = "The implementing class must be able to hire a champion at any point during the game. The player must have enough treasury available to hire and appropriate String messages should be displayed relative to the outcome.";
        test(plyr.hireChampion("Bob Marley").equals("Bob Marley- not found")&&plyr.getMoney()==1000, "Champion doesn�t exist", testing, "Player tries to hire a champion that doesn�t exist.");
        test(plyr.hireChampion("Rudolf").equals("Rudolf- hired and available")&&plyr.getMoney()==600, "Champion exists and enough money", testing, "Player hires a champion with enough money.");
        test(plyr.hireChampion("Argon").equals("Argon- not enough money")&&plyr.getMoney()==600, "Champion exists but player doesn�t have enough treasury", testing, "Player hires a champion but doesn�t have enough treasury to hire the champion.");
        
        /* 6.7 getMoney(): int */
        System.out.println(createTitle('=', MAX_WIDTH, "6.7 getMoney(): int"));
        testing = "This is needed as the player may request to see the state of their treasury.";
        test(plyr.getMoney()==600, "Return treasury", testing, "Player requests to see their treasury in order to determine how much money they have available.");
        
        /* 6.8 getArmy(): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.8 getArmy(): String"));
        testing = "The ability to return a list of all the champions that a player has hired.";
        test(plyr.getArmy()!=null, "Return player Champions", testing, "Player wants to see a list of all of their champions.");
        
        /* 6.9 getAllChallenges(): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.9 getAllChallenges(): String"));
        testing = "The player's ability to retrieve a list of all available challenges as this version of the game currently requires the challenges to be selected by number. A player must also be able to see a list of all possible challenges regardless as per the specification.";
        test(plyr.getAllChallenges()!=null, "Return a list of possible challenges", testing, "A player wants to see the possible challenges that they can accept with their army.");
        
        /* 6.10 meetChallenge(challNo : int): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.10 meetChallenge(challNo : int): String"));
        testing = "The ability to meet challenges is specified in the specification and should return an appropriate String value to tell the user the status of a challenge. Although most return Strings are predefined some situations are not defined by the specification so custom Strings will be returned with an explanation.";
        test(plyr.meetChallenge(1).equals("Challenge won by Rudolf.")&&plyr.getMoney()==700, "Challenge won", testing, "Player accepts challenge and wins.");
        test(plyr.meetChallenge(-1).equals("-1 - no such challenge")&&plyr.getMoney()==700, "Challenge doesn't exist", testing, "Player goes to accept a challenge that doesn't exist for the number they specified.");
        test(plyr.restoreChampion("Rudolf")&&plyr.getMoney()==650, "Pre-condition for next test", testing, "'Rudolph' should be now revived for next test from resting to active.");
        test(plyr.meetChallenge(7).equals("Challenge lost on skill level. <Rudolf> is dead.")&&plyr.getMoney()==450, "Challenge lost on skill level", testing, "Player accepts a challenge that they lose on skill level. Reward money should be deducted from treasury and champion set to dead.");
        test(plyr.meetChallenge(4).equals("Challenge lost as no champion available")&&plyr.getMoney()==250, "Challenge lost as no Champion available", testing, "Player accepts a challenge but there is no Champion available to fight.");
        
        /* 6.11 getChallenge(challengeNum : int): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.11 getChallenge(challengeNum : int): String"));
        testing = "The ability to retrieve a challenge by number. This is expected to be removed in later versions.";
        test(plyr.getChallenge(-1)==null, "Challenge doesn't exist", testing, "Player attempts to retrieve a challenge that doesn't exist.");
        test(plyr.getChallenge(2)!=null, "Challenge exists", testing, "Player correctly requests a player that does exist.");
        
        /* 6.12 toString(): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.12 toString(): String"));
        testing = "Test the implementing classes ability to return player details. This may be requested by the user and is likely to be used on game termination to correctly display the player's resources (everything they own).";
        test(plyr.toString().equals("\nPlayer: Horatio\nTreasury: 250\nYou are still OK \n\n" + plyr.getChampion("Rudolf")), "Return player details", testing, "Player wishes to see their current game status.");
        
        /* 6.13 getAllChampionsForHire(): String */
        System.out.println(createTitle('=', MAX_WIDTH, "6.13 getAllChampionsForHire(): String"));
        testing = "This is to test whether a player is able to request to see all the champions available to hire.";
        test(plyr.getAllChampionsForHire()!=null&&plyr.getAllChampionsForHire().split("\n").length==51, "Player retrieves list of available champions", testing, "Player wishes to view all the champions available to hire in their game. A champion might be dead, active (hired) or resting (hired) and therefore should not be displayed. Only one of these needs to be tested.");
        
        /* 6.14 restoreChampion(String name): boolean */
        System.out.println(createTitle('=', MAX_WIDTH, "6.14 restoreChampion(String name): boolean"));
        testing = "Champion should be able to be restored with a cost of 50 gulden. A champion can only be restored from resting as per the specimen.";
        test(!plyr.restoreChampion("Bob Marley"), "Champion not member of Player's army", testing, "Player attempts to restore a champion that is not a member of Player's army.");
        test(setField(plyr, "treasury", 950), "Pre-condition for next test", testing, "Setting player money to 950 for next tests using reflection.");
        test(plyr.hireChampion("Argon").equals("Argon- hired and available")&&plyr.getMoney()==50, "Pre-condition for next test", testing, "'Argon' should now be an active member of Player's Army.");
        test(!plyr.restoreChampion("Argon")&&plyr.getMoney()==50, "Champion doesn't need restoring", testing, "Player attempts to restore a 'Argon' who does not need restoring and is currently in the state active.");
        test(plyr.meetChallenge(6).equals("Challenge won by Argon.")&&plyr.getMoney()==95, "Pre-condition for next test", testing, "Make Champion need restoring and ultimately lose money.");
        test(plyr.restoreChampion("Argon")&&plyr.getMoney()==45, "Champion restored", testing, "Player successfully restores a resting champion. Treasury is deducted by 50 to reduce it to a total of 45.");
        test(!plyr.restoreChampion("Argon")&&plyr.getMoney()==45, "Not enough money to restore champion", testing, "A player doesn't have enough money to make a restoration regardless of whether restoration is needed.");
        
        /* 6.15 dismissChampion(String name): boolean */
        System.out.println(createTitle('=', MAX_WIDTH, "6.15 dismissChampion(String name): boolean"));
        testing = "At any point during the game a champion can be dismissed for a return of half their hire fee.";
        test(!plyr.dismissChampion("Bob Marley")&&plyr.getMoney()==45, "Champion doesn't exist", testing, "Player attempts to dismiss a champion that doesn't exist.");
        test(!plyr.dismissChampion("Xenon")&&plyr.getMoney()==45, "Champion not member of player's army", testing, "Player attempts to dismiss a champion that is not part of their army.");
        test(plyr.dismissChampion("Argon")&&plyr.getMoney()==495, "Champion dismissed", testing, "Player successfully dismisses a Champion for half of their hire fee.");
        
        /* 6.16 saveGame(filename : String): void */
        System.out.println(createTitle('=', MAX_WIDTH, "6.16 saveGame(filename : String): void"));
        testing = "The game should be able to be saved in order to be loaded from a file again at another point in time.";
        plyr.saveGame("test/new.dat");
        test(plyr.restoreGame("test/new.dat")!=null, "Player save game", testing, "Player saves game with a new file name.");
        plyr.saveGame("test/new.dat");
        test(plyr.restoreGame("test/new.dat")!=null, "File exists", testing, "Player saves game to file that already exists.");
        
        /* 6.17 terminateGame(): String */
        //OMMITED DUE TO NOT BEING A METHOD IN THE CODE INTERFACE//
        
        /* End of Tests */
        System.out.println(createTitle('#', MAX_WIDTH, "End of Tests"));
    }
    
    private void setup(){ plyr = new Player("Horatio"); }
    
    private boolean test(boolean result, String name, String testing, String justification){
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        String msg = "]->" + ste[2].getClassName()
                   + "::" + ste[2].getLineNumber()
                   + "\n--Name--" + format(name, MAX_WIDTH)
                   + "\n--Test--" + format(testing, MAX_WIDTH)
                   + "\n--Justification--" + format(justification, MAX_WIDTH)
                   + "\n" + createLine('-', MAX_WIDTH);
        if(result)System.out.println("[ OK " + msg);
        else{
            System.out.println("[FAIL" + msg);
            throw new AssertionError(result);
        }
        return result;
    }
    
    private String format(String msg, int width){
        String result = "";
        while(msg.length()>width){
            String mini = "\n" + msg.substring(0, width);
            int last = mini.lastIndexOf(' ');
            if(last>0)mini = mini.substring(0, last + 1);
            result += mini;
            msg = msg.substring(mini.length() - 1);
        }
        return result + "\n" + msg;
    }
    
    private String createLine(char chr, int num){
        char[] line = new char[num];
        for(int i=0; i<num; i++)line[i] = chr;
        return new String(line);
    }
    
    private String createTitle(char border, int num, String title){
        String result = createLine(border, num);
        String[] lines = format(title, num - 4).split("\n");
        for(String s : lines){
            if(s.length()>0){
                s = centerString(s, num - 4);
                result += "\n" + border + ' ' + s + createLine(' ', num - s.length() - 4) + ' ' + border;
            }
        }
        return result + "\n" + createLine(border, num) + "\n";
    }
    
    private String centerString(String msg, int width){ return createLine(' ', (width - msg.length()) / 2) + msg; }
    
    private boolean setField(Object inst, String v, Object obj){
        try{
            Field f = inst.getClass().getDeclaredField(v);
            f.setAccessible(true);
            f.set(inst, obj);
            return true;
        }catch(IllegalAccessException e){ return false; }
        catch(NoSuchFieldException e){ return false; }
    }
}
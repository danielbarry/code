import java.io.Serializable;

/**
 * The Warrior class stores the information relative to the Warrior, including the
 * preffered weapon.
 * <br />
 * This class also offers simple methods for accessing and mutating data
 * accordingly with very simple processing. The data it contains is also
 * serializable meaning that an instance can be saved to file.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 21:45 23/02/2014 Prototype
 */
public class Warrior extends Champion implements Serializable{
    /* A set serial number so that when a serialized object is reloaded it reloads
     * with the same UID, even if the JVM is reset. */
    private static final long serialVersionUID = 31L;
    /* The location to attempt to load the default configuration file. */
    private static final String CONSTANT_FILE = "champion_warrior_defaults.config";
    /* Default values to be reffered to on failure to load other values from the
     * configuration file. */
    private static final int DIVIDE_VAL_DEF = 100;
    /* The default values used by this class to define constant values throughout
     * the class. */
    private static int DIVIDE_VAL;
    
    /* Run once initialiser to potentially load in the properties from file. If
     * the values are not found for any reason the default ones specified by the
     * documentation are used. */
    static{
        Property prop = new Property();
        prop.loadFile(CONSTANT_FILE);
        DIVIDE_VAL = prop.loadValue("DIVIDE_VAL", DIVIDE_VAL_DEF, Integer.class);
    }
    
    /* Values that define this Champion from other types of Champion. */
    private String preferedWeapon;
    
    /**
     * This constructor allows an instance of Warrior to be created. The details
     * that define a Warrior are name, hire amount, and their prefered weapon.
     * <br />
     * An attempt is made to laod constant from a properties file if it is
     * possible. These values are automatically casted and do not need to be
     * worried about. If the value if not loaded for any reason, the defaults value
     * is returned instead.
     * 
     * @param name The name that defines the Champion. This must be unique in
     * respect to all Champions. Failing to meet this pre-condition will result in
     * a custom NameExistsException as defined in the Champion class.
     * @param hireAmount This is the amount that the Warrior costs to hire. This
     * value is also used to define how much a Warrior costs by dividing his hire
     * fee by a divide value set to 100 by default.
     * @param preferedWeapon This is a weapon that the Warrior prefers to use.
     * @throws NameExistsException This exception is thrown if the name of the
     * Champion has already been instantiated and currently exists.
     * @throws SkillLevelException This exception is thrown if the skill level of a
     * Champion is less than or greater than the minimum and maxmium accepted
     * levels respectively.
     */
    public Warrior(String name, int hireAmount, String preferedWeapon) throws Champion.NameExistsException, SkillLevelException{
        super(name, (int)(hireAmount / DIVIDE_VAL), hireAmount);
        this.preferedWeapon = preferedWeapon;
    }
    
    /**
     * Returns the Champion's prefered weapon.
     * 
     * @return A value containing the Warrior's prefered weapon.
     */
    public String getWeapon(){ return preferedWeapon; }
    
    /**
     * Returns a message containing the basic information about a Champion,
     * including it's type (which is a String value of child class name), the
     * skill level of the Champion, the amount to hire the Champion and the
     * state the Champion is currently in.
     * 
     * @return A message containing the basic information about the Champion.
     */
    @Override
    public String toString(){
        return super.toString()
           + "\nPref Weapon: " + preferedWeapon;
    }
}
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Scanner;

/**
 * This Class offers a simple procedure to create Challenge Objects and save them
 * to file. This class can be run standalone and excepts user input from the
 * terminal to create and cast a Challenge type object.
 * <br />
 * The reason that Objects are written to file seperately and not part of a
 * collection is that in order for Objects to be added to an existing collection,
 * said collection must be loaded and have the Objects inserted into the
 * collection to garuntee they work correctly. This also makes assumptions about
 * the Collection that will be loading the Objects into the Game reducing the
 * system's versatility.
 * <br />
 * Please note that in order to quit the program loop exit the JVM (program)
 * either via the Operating System's supported method of doing so or by typing
 * 'exit' at any input. An Object is saved once printed to the screen and exiting
 * at this time will not effect the the file.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 01:30 01/03/2014 Prototype
 */
public class ChallengeSetup{
    /**
     * This is a custom adaptation of the ObjectOutputStream to correctly format
     * headers when saving multiple objects to file, as headers get correupt due
     * to there being no reset. This is a known Java issue although whther it will
     * be fixed is unknown.
     * <br />
     * All this method currently does is call the reset method when writing stream
     * headers, allowing multiple objects to be read from a file sequentially.
     * This is a hack as currently the ObjectOutputStream doesn't reset headers
     * correctly causing a header exception to be thrown.
     */
    public class AppendingObjectOutputStream extends ObjectOutputStream{
        /**
         * Creates an ObjectOutputStream that writes to the specified
         * OutputStream. This constructor writes the serialization stream header to
         * the underlying stream; callers may wish to flush the stream immediately
         * to ensure that constructors for receiving ObjectInputStreams will not
         * block when reading the header.
         * <br />
         * If a security manager is installed, this constructor will check for the
         * "enableSubclassImplementation" SerializablePermission when invoked
         * directly or indirectly by the constructor of a subclass which overrides
         * the ObjectOutputStream.putFields or ObjectOutputStream.writeUnshared
         * methods.
         * 
         * @param out output stream to write to
         * @throws IOException if an I/O error occurs while writing stream header
         * @throws SecurityException if untrusted subclass illegally overrides
         * security-sensitive methods
         * @throws NullPointerException if out is null
         */
        public AppendingObjectOutputStream(OutputStream out) throws IOException{ super(out); }
        
        /**
         * The writeStreamHeader method is provided so subclasses can append or
         * prepend their own header to the stream. It writes the magic number and
         * version to the stream.
         * <br />
         * in addition this method also resets the headers meaning multiple
         * objects can be written to a file and read back.
         */
        @Override
        protected void writeStreamHeader() throws IOException{ reset(); }
    }
    
    /* Hard-coded file location of objects to append to. If you wish to add
     * objects to a clean file, please delete it from the file directory. */
    private static final String filename = "challs.dat";
    
    private Scanner scan = new Scanner(System.in);
    
    /**
     * The Challenge Setup's point of entry. Although a String array of arguments
     * is taken from the terminal, currently none of them are used. All values in
     * this program are hard-coded as per the specimen.
     * 
     * @param args Arguments from the Terminal that are currently ignored.
     */
    public static void main(String[] args){ new ChallengeSetup(); }
    
    /**
     * This is the sole constructor as is called by the main method entry point.
     * It takes no paramters as all values are currently hard-coded into this part
     * of the system.
     */
    public ChallengeSetup(){
        System.out.print("\fAdd Challenges to file.\n\nType exit at any time to quit.\n");
        boolean running = true;
        while(running){
            /* Get new Challenge values to be saved to the file. Keep looping
             * through inputs until a correctly formatted one is typed. */
            int challengeNo = getInt("Number:");
            ChallengeType type = getType("Type:");
            int reward = getInt("Reward:");
            int skillLevel = getInt("Skill Level:");
            String enemy = getString("Enemy:");
            /* Attempt to save create and save a new Challenge object to file. An
             * appropriate message is also printed. */
            try{
                Challenge chall = new Challenge(challengeNo, type, reward, skillLevel, enemy);
                System.out.println("\n" + chall.toString());
                System.out.println("\nChallenge " + (writeToFile(chall) ? "" : "not ") + "written to file.");
            }catch(Challenge.ChallengeExistsException e){ System.out.println("\nChallenge already exists."); }
        }
    }
    
    /**
     * Attempt to ammend an Object to file using serialization. Exceptions are
     * caught and ignored in most cases.
     * 
     * @param obj The Object to be written to file using serialization.
     * @return The result of ammending the object to the end of the file.
     */
    private boolean writeToFile(Object obj){
        File file = new File(filename);
        ObjectOutputStream oos = null;
        try{
            if(!file.exists())oos = new ObjectOutputStream(new FileOutputStream(file, true));
            else oos = new AppendingObjectOutputStream(new FileOutputStream(file, true));
            oos.writeObject(obj);
            oos.close();
            return true;
        }catch(FileNotFoundException e){}
        catch(IOException e){}
        try{ oos.close(); }
        catch(IOException e){}
        return false;
    }
    
    /**
     * Returns an Integer, using a passed message to promt the user for input.
     * This method will infinately loop until the user types in a correct value.
     * 
     * @param msg The message to be displayed to the user to prompt them for a
     * correctly formatted and parsable value.
     * @return int The value the user typed in, correctly formatted.
     */
    private int getInt(String msg){
        while(true){
            try{ return Integer.parseInt(getString(msg)); }
            catch(NumberFormatException e){}
        }
    }
    
    /**
     * Returns a type of Challenge and loops infinately until one is recieved. A
     * message is printed to prompt the user for the correct input.
     * 
     * @param msg The message to be displayed to the user to prompt them for a
     * correctly formatted and parsable value.
     * @return The value the user typed in, correctly formatted.
     */
    private ChallengeType getType(String msg){
        while(true){
            try{ return ChallengeType.valueOf(getString(msg).toUpperCase()); }
            catch(IllegalArgumentException e){}
        }
    }
    
    /**
     * Returs a non-null String from the user. Please note this may be blank and
     * may not hold up very well for input paramters for a Challenge object. If
     * 'exit' is typed, program is quit.
     * 
     * @param msg The message to be displayed to the user to prompt them for a
     * correctly formatted and parsable value.
     * @return The value the user typed in, correctly formatted.
     */
    private String getString(String msg){
        System.out.print(msg);
        String str = scan.nextLine();
        if(str.trim().toLowerCase().equals("exit")){
            System.out.println("Program Terminated.");
            System.exit(0);
        }
        return str;
    }
}
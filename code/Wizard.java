import java.io.Serializable;

/**
 * The Wizard class stores the information relative to a Wizard, including whether
 * the Wizard can necromance and the Wizard spell type. If possible, the Wizard
 * attempts to load configuration settings relating to a hire fee dependant on if
 * the Wizard can necromance. If this file fails to load, a defallt file is
 * referenced instead.
 * <br />
 * This class also offers simple methods for accessing and mutating data
 * accordingly with very simple processing. The data it contains is also
 * serializable meaning that an instance can be saved to file.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 21:45 23/02/2014 Prototype
 */
public class Wizard extends Champion implements Serializable{
    /* A set serial number so that when a serialized object is reloaded it reloads
     * with the same UID, even if the JVM is reset. */
    private static final long serialVersionUID = 30L;
    /* The location to attempt to load the default configuration file. */
    private static final String CONSTANT_FILE = "champion_wizard_defaults.config";
    /* Default values to be reffered to on failure to load other values from the
     * configuration file. */
    private static final int CAN_NECRO_DEF = 400, NOT_NECRO_DEF = 300;
    /* The default values used by this class to define constant values throughout
     * the class. */
    private static int CAN_NECRO, NOT_NECRO;
    
    /* Run once initialiser to potentially load in the properties from file. If
     * the values are not found for any reason the default ones specified by the
     * documentation are used. */
    static{
        Property prop = new Property();
        prop.loadFile(CONSTANT_FILE);
        CAN_NECRO = prop.loadValue("CAN_NECRO", CAN_NECRO_DEF, Integer.class);
        NOT_NECRO = prop.loadValue("NOT_NECRO", NOT_NECRO_DEF, Integer.class);
    }
    
    /* Values that define this Champion from other types of Champion. */
    private boolean necromancer;
    private String spellType;
    
    /**
     * This constructor allows an instance of Wizard to be created. The details
     * that define a Wizard are name, skill level, whether they necromance and a
     * spell type.
     * <br />
     * An attempt is made to laod constant from a properties file if it is
     * possible. These values are automatically casted and do not need to be
     * worried about. If the value if not loaded for any reason, the defaults value
     * is returned instead.
     * 
     * @param name The name that defines the Champion. This must be unique in
     * respect to all Champions. Failing to meet this pre-condition will result in
     * a custom NameExistsException as defined in the Champion class.
     * @param skillLevel This defines the skill level of the Wizard. Please note
     * that it is important to keep within the specified constraints of an accepted
     * skill level value. Failing to do so will result in a custom Skill Level
     * Exception being thrown.
     * @param necromancer This value defines whether the Wizard can necromance,
     * True representing yes and False no. By default, not being a necromancer sets
     * the hire fee to 300 whereas being a necromancer will set the hire fee to
     * 400.
     * @param spellType The special spell type of the Wizard.
     * @throws NameExistsException This exception is thrown if the name of the
     * Champion has already been instantiated and currently exists.
     * @throws SkillLevelException This exception is thrown if the skill level of a
     * Champion is less than or greater than the minimum and maxmium accepted
     * levels respectively.
     */
    public Wizard(String name, int skillLevel, boolean necromancer, String spellType) throws Champion.NameExistsException, SkillLevelException{
        super(name, skillLevel, necromancer ? CAN_NECRO : NOT_NECRO);
        this.necromancer = necromancer;
        this.spellType = spellType;
    }
    
    /**
     * Returns the Wizard's spell type value.
     * 
     * @return The value of the Wizard's spell type.
     */
    public String getSpellType(){ return spellType; }
    
    /**
     * Returns whether the Wizard can necromance as a Boolean value.
     * 
     * @return Whether a Wizard can necromance.
     */
    public boolean canNecromance(){ return necromancer; }
    
    /**
     * Returns a message containing the basic information about a Champion,
     * including it's type (which is a String value of child class name), the
     * skill level of the Champion, the amount to hire the Champion and the
     * state the Champion is currently in.
     * 
     * @return A message containing the basic information about the Champion.
     */
    @Override
    public String toString(){
        return super.toString()
           + "\nNecromancer: " + necromancer
           + "\nSpell Type : " + spellType;
    }
}
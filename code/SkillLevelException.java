/**
 * This has been created to allow programmers using the Champion class to get
 * feedback on Champion creation. This particular Exception is thrown if a
 * Champion's skill level is not within the set bounds of the maximum and
 * minimum allowed to exist.
 * 
 * @author Daniel Barry & Paul Moggridge
 * @version 1.1 21:10 03/03/2014 Prototype
 */
public class SkillLevelException extends NumberFormatException{
    /**
     * This constructor handles the details of the Exception with a message
     * containing detials about the particular instance.
     * 
     * @param s The message to be printed explaining the Exception thrown.
     */
    public SkillLevelException(String s){ super(s); }
}
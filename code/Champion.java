import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Properties;

/**
 * The Champion class is a means of defining Champions that can be used throughout
 * the game, offering useful methods for simple game play. Champions may be set to
 * one of four states as defined in the ChampionState enumerator:
 * <ul>
 *   <li>For Hire � is in the game�s collection of champions but not yet hired for
 *   the Player's army</li>
 *   <li>Active - has been hired for the Player's army and is available to meet
 *   challenges</li>
 *   <li>Resting - has been hired for the Player's army, is not available to meet
 *   challenges, but can be restored</li>
 *   <li>Dead � is not available for challenges, cannot be restored</li>
 * </ul>
 * A Champion is also able to return their hire fee, as well as a simple String
 * representation of their current state.
 * <br />
 * This class has been designed with the desire to reduce coupling within the
 * overall system and to throw custom Exceptions in the constructor where data is
 * recieved in the wrong format.
 *
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 04:45 23/02/2014 Prototype
 */
public abstract class Champion implements Serializable{
    /* A set serial number so that when a serialized object is reloaded it reloads
     * with the same UID, even if the JVM is reset. */
    private static final long serialVersionUID = 3L;
    /* An ArrayList that keeps a weak refference to Champion instances, in which
     * can be used to check whether a Champion created has a true unique
     * identifier name as per the specification. */
    private static ArrayList<WeakReference<Champion>> instances = new ArrayList<WeakReference<Champion>>();
    /* The location to attempt to load the default champions configuration. */
    private static final String CONSTANT_FILE = "champion_defaults.config";
    /* The values to default to if values from the configuration can't be
     * loaded. */
    private static final int MIN_SKILL_LEVEL_DEF = 1, MAX_SKILL_LEVEL_DEF = 10;
    /* The values to be used to check the skill level boundaries that a Champion is
     * expected to be able to have. */
    private static int MIN_SKILL_LEVEL, MAX_SKILL_LEVEL;
    
    /* Initializer setup to load the values of MIN_SKILL_LEVEL and MAX_SKILL_LEVEL
     * from a file if possible. Any excpetions that occur from this have been
     * declared as caught and are returned to their default values defined by
     * MIN_SKILL_LEVEL_DEF and MAX_SKILL_LEVEL_DEF. */
    static{
        /* Attempt to load the default properties file with the values. On failure,
         * just use the ones specified. */
        Property prop = new Property();
        prop.loadFile(CONSTANT_FILE);
        MIN_SKILL_LEVEL = prop.loadValue("MIN_SKILL_LEVEL", MIN_SKILL_LEVEL_DEF, Integer.class);
        MAX_SKILL_LEVEL = prop.loadValue("MAX_SKILL_LEVEL", MAX_SKILL_LEVEL_DEF, Integer.class);
    }
    
    /* The name of a Champion is a unique identifier as per the specimen and is
     * therefore checked to make sure that it is unique. */
    private String name;
    /* The skill level is an integer between the minimum and maximum specified
     * values for a valid skill amount. */
    private int skillLevel;
    /* The amount a Champion costs to hire. */
    private int hireAmount;
    /* The current state of the Champion, consisting of the states specified in
     * the ChampionState enumerator. */
    private ChampionState state;
    
    /**
     * This has been created to allow programmers using the Champion class to get
     * feedback on Champion creation. This particular Exception is thrown if a
     * Champion's name already exists, as this breaks the rule of a Champion having
     * a unique identifier as a name.
     */
    public class NameExistsException extends Exception{
        /**
         * This constructor handles the details of the Exception with a message
         * containing detials about the particular instance.
         * 
         * @param s The message to be printed explaining the Exception thrown.
         */
        public NameExistsException(String s){ super(s); }
    }
    
    /**
     * Sole constructor. This constructor cannot be invoked by a programmer and
     * must be inherited. The basic information required to describe a Champion of
     * any type is expected.
     * <br />
     * Champions are currently checked to make sure they are uniquely identified.
     * 
     * @param name The name of the Champion which is also a unique identifier.
     * @param skillLevel Skill Level of the Champion, defined as a number between
     * and including 1 to 10. Exceptions are thrown indicating when these rules
     * have been broken.
     * @param hireAmount The amount that the Champion costs to hire.
     * @throws NameExistsException This exception is thrown if the name of the
     * Champion has already been instantiated and currently exists.
     * @throws SkillLevelException This exception is thrown if the skill level of a
     * Champion is less than or greater than the minimum and maxmium accepted
     * levels respectively.
     */
    public Champion(String name, int skillLevel, int hireAmount) throws NameExistsException, SkillLevelException{
        /* Save the value of the passed name to this Champion */
        this.name = name;
        /* Check this Champion's name to find out whether it is a unique
         * identifier. Whilst checking, it is important that if we come across a
         * weak reference that it is removed from the ArrayList to save space and
         * future time. */
        for(WeakReference<Champion> champion : instances){
            Champion champ = champion.get();
            if(champ!=null){
                /* Make sure that the refference isn't weak before attempting
                 * check. */
                if(champ.name.equals(this.name)){
                    /* Output a disruptive error message to the programmer for using the
                     * Champion incorrectly. */
                    throw new NameExistsException("Unique identifier of Champion already exists.");
                }
            }else{
                /* If reference is weak, remove it from the ArrayList so we don't
                 * waste time or space with it again. */
                instances.remove(champ);
            }
        }
        /* Add this instance to a collection of all instances. Keep reference to
         * whole instance as it may prove useful later and doesn't add noticeable
         * overhead. The reason for using a Weak Reference in this case is to
         * reduce coupling throughout the whole program as per the specification. */
        instances.add(new WeakReference<Champion>(this));
        this.skillLevel = skillLevel;
        if(this.skillLevel<MIN_SKILL_LEVEL||this.skillLevel>MAX_SKILL_LEVEL){
            /* Output a disruptive error message to the programmer for using the
             * Champion incorrectly. */
            throw new SkillLevelException("Skill level is not allowed. Must be between " + MIN_SKILL_LEVEL + " and " + MAX_SKILL_LEVEL + ".");
        }
        this.hireAmount = hireAmount;
        /* Assumption that Champion will be set automatically available as Champion
         * must start with a state. */
        setForHire();
    }
    
    /**
     * The name of the Champion should be returnable as it is the unique
     * identifier.
     * 
     * @return The name of the Champion.
     */
    public String getName(){ return name; }
    
    /**
     * The skill level of the Champion is returned. This could be for the prupose
     * of checking whether a Challenge could be won based on skill level for example.
     * 
     * @return The skill level of the Champion.
     */
    public int getSkillLevel(){ return skillLevel; }
    
    /**
     * The hire fee amount can be returned for a Champion.
     * 
     * @return The hire fee amount for an instance of Champion.
     */
    public int getHireFee(){ return hireAmount; }
    
    /**
     * For Hire state can be set. This denotes that the Champion is available for
     * hire.
     */
    public void setForHire(){ state = ChampionState.FORHIRE; }
    
    /**
     * Active state can be set. This denotes that the Champion is available to
     * fight.
     */
    public void setActive(){ state = ChampionState.ACTIVE; }
    
    /**
     * Resting state can be set. This denotes that the Champion is currently
     * resting as a result of a recent fight.
     */
    public void setResting(){ state = ChampionState.RESTING; }
    
    /**
     * Dead state can be set. This denotes that the Champion is currently
     * unavailable and is dead. This can be the result of multiple occurences.
     */
    public void setDead(){ state = ChampionState.DEAD; }
    
    /**
     * Returns the whether the Champion is for hire.
     * 
     * @return If the Champion is for hire, true is returned otherwise false;
     */
    public boolean isForHire(){ return state==ChampionState.FORHIRE; }
    
    /**
     * Returns the whether the Champion is active.
     * 
     * @return If the Champion is active, true is returned otherwise false;
     */
    public boolean isActive(){ return state==ChampionState.ACTIVE; }
    
    /**
     * Returns the whether the Champion is resting.
     * 
     * @return If the Champion is resting, true is returned otherwise false;
     */
    public boolean isResting(){ return state==ChampionState.RESTING; }
    
    /**
     * Returns the whether the Champion is dead.
     * 
     * @return If the Champion is dead, true is returned otherwise false;
     */
    public boolean isDead(){ return state==ChampionState.DEAD; }
    
    /**
     * Returns a message containing the basic information about a Champion,
     * including it's type (which is a String value of child class name), the
     * skill level of the Champion, the amount to hire the Champion and the
     * state the Champion is currently in.
     * 
     * @return A message containing the basic information about the Champion.
     */
    @Override
    public String toString(){
        return "\nName       : " + name
           + "\nSkill Level: " + skillLevel
           + "\nHire Amount: " + hireAmount
           + "\nState      : " + state.toString();
    }
}
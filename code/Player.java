import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class implements the behaviour expected from the CODE
 * system as required for 5COM0087 Cwk 3 - Feb 2014
 * 
 * @author Daniel Barry & Paul Moggridge
 * @version 1.1 02:00 01/03/2014 Prototype
 */
public class Player implements Game, Serializable{
    /* A set serial number so that when a serialized object is reloaded it reloads
     * with the same UID, even if the JVM is reset. */
    private static final long serialVersionUID = 1L;
    /* Array List of all children classes to Champion. This is to be used as an
     * indexing system for the LUT (Look Up Table) for determining whether a fight
     * is to take place based on a Champion's ability to take part in that fight.
     * A reference to the child's class is kept as this can be directly compared
     * with the class of the Champion when being compared for a fight.
     * Note: Due to the nature of Java, there is no easy way to detect loaded
     * Child classes in the JVM as they either need to be detected on the disk
     * (slow and relies on prior knowledge of the package layout), Instantiation
     * class which relies on a custom Instantiation being loaded meaning it won't
     * run correctly in a default BlueJ environment, Itterated through class
     * loaders that are only aware of their Parent loaders and not Children
     * loaders and a few other very obscure methods. The simple fact is Java does
     * not want you to dynamically be aware of a classes Children classes at
     * RunTime. */
    private static ArrayList<Class<?>> champions = new ArrayList<Class<?>>();
    /* LUT (Look Up Table) to be used to determine whether a Champion can
     * participate in a Challenge. This is done by using a 2 dimensional array of
     * boolean values indexed by the Challenge Type and Champion children
     * classes.
     * Note: A LUT was used due to the nature of the data and the said potential
     * in the documentation to expand the given table if another Champion is added
     * to the system. An LUT is both faster to calculate in most cases and simpler
     * to view and update. */
    private static boolean LUT[][];
    /* Static initializer set aside to determine whether a Champion can
     * participate in a Challenge. This is done by using a 2 dimensional array of
     * boolean values indexed by the Challenge Type and Champion children
     * classes. An Array List containing the children (adapted manually by a
     * programmer as RunTime realization proved too difficult) is setup, where
     * the indexed location is used as part of the indexing system for the LUT.
     * The LUT is then built using these indexes. */
    static{
        champions.add(Wizard.class);
        champions.add(Warrior.class);
        champions.add(Dragon.class);
        LUT = new boolean[ChallengeType.values().length][champions.size()];
        //                                                  |Wizard |Warrior|Dragon|
        LUT[ChallengeType.MAGIC.ordinal()] =   new boolean[]{true,   false,  true  };
        LUT[ChallengeType.FIGHT.ordinal()] =   new boolean[]{false,  true,   true  };
        LUT[ChallengeType.MYSTERY.ordinal()] = new boolean[]{false,  true,   true  };
    }
    
    private String name;
    private int treasury;
    /* All Champions in the game. Note the strong reference. */
    private HashMap<String, Champion> allChampions;
    /* All challenges in the game. Please note the strong reference.
     * Note: All Challenges are referenced via a HashMap as the incremented Integer
     * isn't explicitly auto-incremented. Technically speaking, an Integer of 99
     * could be added to the list with only 3 previous entries existing. These
     * entries need to be referenced by number which is why a Map is used. */
    private HashMap<Integer, Challenge> allChallenges;
    /* Available champions for hire */
    private HashMap<String, Champion> availableChampions;
    /* Player's champions */
    private HashMap<String, Champion> playerArmy;
    /* Challenges available */
    private HashMap<Integer, Challenge> availableChallenges;

    //**************** DiscEarth ************************** 
    /**
     * Constructor requires the name of the player
     * 
     * @param player is the name of the player
     */  
    public Player(String player){
        name = player;
        treasury = 1000;
        
        allChampions = new HashMap<String, Champion>();
        allChallenges = new HashMap<Integer, Challenge>();
        availableChampions = new HashMap<String, Champion>();
        playerArmy = new HashMap<String, Champion>();
        availableChallenges = new HashMap<Integer, Challenge>();
        
        setupChampions();
        setupChallenges();
    }
    
    /**
     * Constructor requires the name of the player and the
     * name of the file storing champions
     * 
     * @param player is the name of the player
     * @param filename name of file storing champions
     */  
    public Player(String player, String filename){
        name = player;
        treasury = 1000;
        
        allChampions = new HashMap<String, Champion>();
        allChallenges = new HashMap<Integer, Challenge>();
        availableChampions = new HashMap<String, Champion>();
        playerArmy = new HashMap<String, Champion>();
        availableChallenges = new HashMap<Integer, Challenge>();
        
        readChampions(filename); //Read from text file
        readChallenges();
    }
    
    /**
     * Returns the name of the player
     * 
     * @return is the name of the player 
     **/ 
    public String getName(){ return name; }
    
    /**
     * Returns a String representation of the state of the game,
     * including the name of the player, state of the treasury,whether 
     * player has lost or is still OK, and the champions in the army 
     * (or, "No champions" if army is empty)
     * 
     * @return a String representation of the state of the game,
     * including the name of the player, state of the treasury, whether 
     * player has lost or is still OK, and the champions in the army 
     * (or, "No champions" if army is empty)
     **/
    @Override
    public String toString(){
        String s = "\nPlayer: " + name ;
        s = s + "\nTreasury: " + treasury;        
        if (hasLost())s = s + "\nYou have lost \n" ;
        else s = s + "\nYou are still OK \n" ;
        /* Add the army to this String, or if empty "no Champions in the army" */
        s += playerArmy.size()>0 ? getAbstractMapString(playerArmy) : "no Champions in the army";
        return s;
    }
    
    /**
     * Returns the amount of money in the treasury
     * 
     * @returns the amount of money in the treasury
     */
    public int getMoney(){ return treasury; }

    /**
     * Returns true if treasury <=0 and the army has no champions 
     * who can be dismissed. 
     * 
     * @returns true if treasury <=0 and the army has no champions 
     * who can be dismissed. 
     */
    public boolean hasLost(){
        boolean active = false;
        for(Champion champ : playerArmy.values())if(champ.isActive()||champ.isResting())active = true;
        return treasury<=0&&!active;
    }

    //***************** Army of Champions ************************  
    /**
     * Returns a String representation of all champions available for hire
     * 
     * @return a String representation of all champions available for hire
     **/
    public String getAllChampionsForHire(){ return getAbstractMapString(availableChampions); }
    
    /**
     * Returns a formatted String containing a String representation of all the
     * Objects found in the passed AbstractMap. The AbstractMap must contain
     * values that the toString() method can be called on. These should ideally be
     * formatted so they can be returned with all the intended information
     * relative to the Object.
     * 
     * @param data The Abstract Map to itterate through to collect the toString()
     * details.
     * @return A newline ("\n") formatted String containing the values of the
     * Abstract Map.
     */
    private String getAbstractMapString(AbstractMap<?, ?> data){
        String val = "";
        /* Cast to type Object, as all Objects inherit inherit the toString()
         * method. */
        for(Object obj : data.values())val += "\n" + obj.toString();
        return val;
    }
    
    /**
     * Returns details of a champion with the given name
     * 
     * @return details of a champion with the given name
     **/
    public String getChampion(String name){
        String nme = name.toLowerCase();
        Champion champ = allChampions.get(nme);
        return champ!=null ? champ.toString() : null;
    }
    
    //***************** Army Champions ************************   
    /**
     * Allows a champion to be hired for the army, if there 
     * is enough money in the treasury for their hire fee.The champion's 
     * state is set to "active"
     * 
     * @param name is the name of the champion
     * @return name and either "- not found" if champion not found,or "- cannot 
     * be hired" if champion is not for hire,already hired/dead, "- hired and 
     * available" if champion hired, "- not enough money" if not enough money
     * in the treasury
     **/        
    public String hireChampion(String name){
        String nme = name.toLowerCase();
        /* Make sure that Champion exists. */
        if(!allChampions.containsKey(nme))return name + "- not found";
        Champion champ = availableChampions.get(nme);
        if(champ==null)return name + "- cannot be hired";
        else if(champ.isDead())return name + "- cannot be hired";
        /* Make sure Champion exists, is not dead and there is enough treasury. */
        if(treasury<champ.getHireFee())return name + "- not enough money";
        /* Deduct Treasury and add Champion to army, removing them from the list
         * of available Champions. */
        treasury -= champ.getHireFee();
        availableChampions.remove(nme);
        playerArmy.put(nme, champ);
        champ.setActive();
        return name + "- hired and available";
    }
        
    /**
     * Returns a String representation of the champions in the player's army
     * with an appropriate header, or the message "No champions hired"
     * 
     * @return a String representation of the champions in the player's army
     **/
    public String getArmy(){ return getAbstractMapString(playerArmy); }
    
    /**
     * Returns true if the champion with the name is in the player's army, 
     * false otherwise.
     * 
     * @param name is the name of the champion
     * @return true if the champion with the name is in the player's army, 
     * false otherwise.
     **/
    public boolean isInArmy(String name){
        String nme = name.toLowerCase();
        /* Make sure Champion exists and has not been set to null. */
        if(playerArmy.containsKey(nme.toLowerCase())){
            if(playerArmy.get(nme.toLowerCase())!=null)return true;
        }
        return false;
    }
    
    /**
     * Dismisses a champion from the army and add half of their hire fee 
     * to the treasury.Champion must be active or resting.Champion should
     * now be for hire.
     * pre-condition: isInArmy(name)and is not dead
     * 
     * @param name is the name of the champion
     * @return true if dismissed, else false
     **/
    public boolean dismissChampion(String name){
        String nme = name.toLowerCase();
        /* Make sure Champion is member of Army. */
        if(isInArmy(nme)){
            Champion champ = playerArmy.get(nme);
            /* Make sure Champion is either Active or Resting. */
            if(champ.isActive()||champ.isResting()){
                /* Return half of original cost. */
                treasury += champ.getHireFee() / 2;
                /* Remove Champion from Player army. */
                playerArmy.remove(nme);
                /* Add Champion to available Champions list and set 'For Hire'. */
                availableChampions.put(champ.getName().toLowerCase(), champ);
                champ.setForHire();
                return true;
            }
        }
        return false;
    }
    
    /**
     * Restores a champion to the army by setting their state to ACTIVE
     * 
     * @param the name of the champion to be restored
     * @return true if restored, else false
     */
    public boolean restoreChampion(String name){
        String nme = name.toLowerCase();
        /* Make sure Champion is member of Army. */
        if(isInArmy(nme)){
            Champion champ = playerArmy.get(nme);
            /* Make sure enough treasury is available. */
            if(treasury>=50){
                /* Make sure Champion needs restoring. */
                if(champ.isResting()){
                    /* Restore Champion. */
                    treasury -= 50;
                    champ.setActive();
                    return true;
                }
            }
        }
        return false;
    }
    
    //**********************Challenges************************* 
    /**
     * Returns true if the number represents a challenge
     * 
     * @param num is the reference number of the challenge
     * @returns true if the reference number represents a challenge
     **/
    public boolean isChallenge(int num){
        /* Check for null is needed as Hashmapt will return null if key is not
         * found. */
        return allChallenges.get(num)!=null;
    }
    
    /**
     * Meets the challenge represented by the challenge number (or returns 
     * " - no such challenge").Find a champion from the army who can meet the 
     * challenge and return a result which is one of the following: “Challenge 
     * won by...“ – add reward to treasury, set the champion to resting and add 
     * the name of champion, “Challenge lost as no champion available” – deduct 
     * reward from treasury,“Challenge lost on skill level”- deduct reward from 
     * treasury, the champion is killed, so add "<champion name> is dead" to the 
     * return String. If the challenge is lost and there is no money left, add 
     * "You have NO money in the treasury".
     * 
     * @param challNo is the reference number of the challenge
     * @return a String showing the result of meeting the challenge
     */
    public String meetChallenge(int challNo){
        /* Check to see if Challenge exists. If Challenge doesn't exist return
         * message. */
        if(!isChallenge(challNo))return challNo + " - no such challenge";
        /* If Challenge exists then store it. */
        Challenge chall = availableChallenges.get(challNo);
        /* Check to see if returned Champion exists. */
        if(chall==null)return challNo + " - challenge not available";
        for(Champion champ : playerArmy.values()){
            /* Search for a Champion that can fight. */
            if(canFight(chall, champ)){
                /* Return the status of the fight had. */
                return fight(chall, champ);
            }
        }
        /* Deduct reward from treasury as no Champion found */
        String result = "Challenge lost as no champion available";
        return result += updateTreasury(-chall.getRewardAmount());
    }
    
    /**
     * Makes a Champion fight in a Challenge and returns a String indicating the
     * result of the fight and a warning message about a zero or less treasury
     * amount if applicable to the Player. The pre-condition is that there has
     * already been a check to see whether a Player is allowed to fight in a
     * Challenge - taking into consideration the relevant types.
     * 
     * @param chall The Challenge that the Player is fighting in.
     * @param champ The Champion fighting in the Challenge.
     * @return A message about the result of the fight, including details about the
     * state of the treasury if there in no money left.
     */
    private String fight(Challenge chall, Champion champ){
        String result = "";
        /* Check skill level. */
        if(champ.getSkillLevel()>=chall.getSkillLevel()){
            /* Fight won, set Champion resting and add reward. */
            champ.setResting();
            result += "Challenge won by " + champ.getName() + '.';
            result += updateTreasury(chall.getRewardAmount());
        }else{
            /* Fight lost, set Champion dead and remove reward. */
            champ.setDead();
            result += "Challenge lost on skill level. <" + champ.getName() + "> is dead.";
            result += updateTreasury(-chall.getRewardAmount());
        }
        /* Return the result of the fight. */
        return result;
    }
    
    /**
     * Updates the treasury with the given amount and returns a message only if the
     * treasury amout is zero or less - otherwise returns an empty String.
     * 
     * @param amount The amount to add to the treasury (adding a negative amount
     * will deduct from the treasury).
     * @return A message only if the Player's treasury is equal to or less than
     * zero, otherwise a blank String returned.
     */
    private String updateTreasury(int amount){
        /* Add treasury and return message if treasury low. */
        treasury += amount;
        return treasury<=0 ? " You have NO money in the treasury." : "";
    }
    
    /**
     * Checks the LUT (Look Up Table) to see whether a Challenge can be met by a
     * Champion - NOT whether a Challenge is won. Please refer to the
     * documentation or the initialization of the LUT comments in this Classes
     * source code for more information on the exact values.
     * 
     * @param chall The Challenge that the Player is attempting to meet.
     * @param champ The Champion being compared to find out whether they are able
     * to meet the Challenge in question.
     * @return A value representing whether the specified Champion can fight in the
     * Challenge given. True represents a fight being possible, whereas false means
     * the Champion is not able to fight in the Challenge.
     */
    private boolean canFight(Challenge chall, Champion champ){
        /* If Champion is not active, Champion is not in a state to fight and
         * therefore fails to meet the conditions required to meet the
         * Challenge. */
        if(!champ.isActive())return false;
        ChallengeType ct = chall.getType();
        boolean result = LUT[ct.ordinal()][champions.indexOf(champ.getClass())];
        if(champ instanceof Dragon)result = result && ((Dragon)champ).canTalk();
        return result;
    }

    /**
     * Provides a String representation of a challenge given by challenge number
     * pre-condition isChallenge(num)
     * 
     * @param num the number of the challenge
     * @return returns a String representation of a challenge given by 
     * the challenge number
     **/
    public String getChallenge(int num){
        Challenge chall = allChallenges.get(num);
        return chall!=null ? chall.toString() : null;
    }
    
    /**
     * Provides a String representation of all requests
     * 
     * @return returns a String representation of of all requests
     **/
    public String getAllChallenges(){ return getAbstractMapString(allChallenges); }
    
    //*********************** Task 1 ***********************************************
    /**
     * Sets up all Champions in the system. A pre-condition is that the name is
     * unique, although this is checked and throws a RunTime exception anyway.
     * This is not declared as an @throws annotation as it is not meant to be
     * caught, as it could potentially allow the system to include duplicates of
     * unique identifiers.
     */
    private void setupChampions(){
        /* Add the required Champions for a default load. A Champion that is given
         * a non-unique name will throw a custom Name Exists Exception, in which
         * case a custom run time excpetion is thrown. */
        try{
            allChampions.put("ganfrank", new Wizard("Ganfrank", 7, true, "transmutation"));
            allChampions.put("rudolf", new Wizard("Rudolf", 6, true, "invisibility"));
            allChampions.put("elblond", new Warrior("Elblond", 200, "sword"));
            allChampions.put("flimsi", new Warrior("Flimsi", 300, "bow"));
            allChampions.put("drabina", new Dragon("Drabina", false));
            allChampions.put("golum", new Dragon("Golum", true));
            allChampions.put("argon", new Warrior("Argon", 900, "mace"));
            allChampions.put("neon", new Wizard("Neon", 2, false, "translocation"));
            allChampions.put("xenon", new Dragon("Xenon", true));
            availableChampions.putAll(allChampions);
        }catch(Champion.NameExistsException e){
            /* If name is not a unique identifier, in this case to alert the
             * programmer that their hard-coded data is invalid we will throw a
             * custom run time exception to alert them to an error. */
            throw new RuntimeException("Name already exists. Name must be a unique identifier.");
        }
    }
    
    /**
     * Sets up all Challenges in the system. A pre-condition is that the nnumber is
     * unique, although this is checked and throws a RunTime exception anyway.
     * This is not declared as an @throws annotation as it is not meant to be
     * caught, as it could potentially allow the system to include duplicates of
     * unique identifiers.
     */
    private void setupChallenges(){
        try{
            allChallenges.put(1, new Challenge(1, ChallengeType.MAGIC, 100, 3, "Borg"));
            allChallenges.put(2, new Challenge(2, ChallengeType.FIGHT, 120, 3, "Huns"));
            allChallenges.put(3, new Challenge(3, ChallengeType.MYSTERY, 150, 3, "Ferengi"));
            allChallenges.put(4, new Challenge(4, ChallengeType.MAGIC, 200, 9, "Vandals"));
            allChallenges.put(5, new Challenge(5, ChallengeType.MYSTERY, 90, 7, "Borg"));
            allChallenges.put(6, new Challenge(6, ChallengeType.FIGHT, 45, 8, "Goths"));
            allChallenges.put(7, new Challenge(7, ChallengeType.MAGIC, 200, 10, "Franks"));
            allChallenges.put(8, new Challenge(8, ChallengeType.FIGHT, 150, 10, "Sith"));
            availableChallenges.putAll(allChallenges);
        }catch(Challenge.ChallengeExistsException e){
            /* If number is not a unique identifier, in this case to alert the
             * programmer that their hard-coded data is invalid we will throw a
             * custom run time exception to alert them to an error. */
            throw new RuntimeException("Number already exists. Number must be a unique identifier.");
        }
    }
    
    //************************ Task 5 ************************************************
    /**
     * Reads data about champions from a text file and stores in collection of 
     * champions.Data in the file is  "comma separated" and so editable
     * 
     * @param fileName name of the file to be read
     */   
    private void readChampions(String fileName){
        /* Catch exceptions upon file load and data casting. We're not to bothered
         * about how it fails, as there are so many different scenarios in which
         * it can fail that recovery would be very difficult and beyond the scope
         * of this project. Possible failures in given scenarios are considered
         * and caught so that the rest of the program doesn't suffer from a failed
         * to read or cast. */
        try{
            /* Open our file ready to be read line by line. */
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            while((line = br.readLine())!=null){
                /* Split the read line by all commas, as these define a comma
                 * seperated file. */
                String[] data = line.split(",");
                /* Get the class from the name of the class using the Class Loader
                 * (in the background). Please note that class names are read in
                 * binary and are case sensitive. */
                Class<?> clazz = Class.forName(Character.toUpperCase(data[0].charAt(0)) + data[0].substring(1));
                /* Get all possible constructors from the class we just found. This
                 * will be used to compare the data we currently have to the
                 * parameters of each of the constructors. This all relies on the
                 * previous class being found, where an exception will be thrown at
                 * this point if it wasn't found. */
                Constructor<?>[] ctors = clazz.getDeclaredConstructors();
                /* Set the constructor to be used as the first constructor found
                 * initially so that there is somthing to use regardless. */
                Constructor<?> ctor = ctors[0];
                for(Constructor c : ctors){
                    /* Try to find a constructor with the same number of potential
                     * paramters that we currently have. */
                    if(c.getParameterTypes().length==data.length-1){
                        ctor = c;
                        break;
                    }
                }
                /* Get the paramter types of our selected constructor and setup an
                 * Object array for storing the casted values. */
                Class<?>[] paramTypes = ctor.getParameterTypes();
                Object[] params = new Object[paramTypes.length];
                for(int x=0; x<paramTypes.length; x++){
                    /* Make sure that we get the Class for a primitive, as Java
                     * does not currently autobox to the correct class type when
                     * using '.class'. If we have a primtive, lookup the class
                     * using the primLookup method. */
                    if(paramTypes[x].isPrimitive())paramTypes[x] = primLookup(paramTypes[x]);
                    /* Cast the string data to the given type. Notice how we
                     * don't handle any conversion errors. This is is because at
                     * current converting to the type is the best and only method,
                     * so handling these errors properley is pointless as we
                     * wouldn't be able to do anything with them anyway. */
                    params[x] = paramTypes[x].getConstructor(String.class).newInstance(data[x + 1]);
                }
                /* Using our newly formed paramters, create an instance using the
                 * constructor to the Champion class. */
                Champion champ = (Champion)(ctor.newInstance(params));
                /* Add Champion to relative collections ready for the game. */
                allChampions.put(champ.getName().toLowerCase(), champ);
                if(champ.isForHire())availableChampions.put(champ.getName().toLowerCase(), champ);
                else playerArmy.put(champ.getName().toLowerCase(), champ);
            }
            /* On making it this far, we can close the file as we are now done
             * with it. */
            br.close();
            /* We catch exceptions individually for two reasons: 1. We know what
             * exceptions are actually being thrown, and it proves to ourselves
             * that we know what is going on. 2. In the future we may be able to
             * handle these exceptions separetly given more time. */
        }catch(ClassNotFoundException e){}
        catch(IllegalAccessException e){}
        catch(InstantiationException e){}
        catch(InvocationTargetException e){}
        catch(IOException e){}
        catch(NoClassDefFoundError e){}
        catch(NoSuchMethodException e){}
        catch(NullPointerException e){}
    }
    
    /**
     * Allows a Java primitive to be cast to it's wrapper class. There is
     * currently (at time of writing) no supported method for conversion of
     * primtive to wrapper class. Autoboxing also doesn't allow for a primtive
     * to cast a String using it's wrapper class, meaning that this method or
     * others like it is the only well known main stream method of doing so.
     * 
     * @param clazz The primitive type that we want to cast to it's larger and
     * more able over-head class. This must be pre-checked, otherwise null is
     * returned indicating that type passed is not a primitive.
     * @return The Class that represents the Java primtive value. If the
     * primitive cannot be found, null is returned indicating it couldn't be
     * cast.
     */
    private Class<?> primLookup(Class<?> clazz){
        if(clazz.isAssignableFrom(byte.class))return Byte.class;
        if(clazz.isAssignableFrom(short.class))return Short.class;
        if(clazz.isAssignableFrom(int.class))return Integer.class;
        if(clazz.isAssignableFrom(long.class))return Long.class;
        if(clazz.isAssignableFrom(float.class))return Float.class;
        if(clazz.isAssignableFrom(double.class))return Double.class;
        if(clazz.isAssignableFrom(boolean.class))return Boolean.class;
        if(clazz.isAssignableFrom(char.class))return Character.class;
        return null;
    }
    
    //************************ Task 6 **********************************
    /**
     * Reads data about challenges from an object file and stores in collection of 
     * Challnges in a file that is not editable.
     */
    private void readChallenges(){
        /* Load hardcoded path name from file. */
        String filename = "challs.dat";
        try{
            /* Open a Object Input Stream to load the objects in from file. */
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
            try{
                /* Infinately try and load more objects from file until either
                 * a read error of some sorts or an EOFException is thrown. As
                 * can be seen, this section catches an EOFException and
                 * recognises the fact the file then needs to be closed as a
                 * result, as all objects have been read. */
                while(true){
                    /* Attempt to cast the read Object as a Champion and
                     * insert the object into the main Challenges
                     * collection. */
                    Challenge chall = (Challenge)(ois.readObject());
                    allChallenges.put(chall.getChallengeNo(), chall);
                }
            }catch(EOFException e){ ois.close(); }
        }catch(ClassNotFoundException e){}
        catch(FileNotFoundException e){}
        catch(IOException e){}
        /* It is assumed that this data is read at the start of the program and
         * therefore means that data must be available. Handling methods for
         * completing competitions has not yet been properley specified. */
        availableChallenges.putAll(allChallenges);
    }
    
    //********************   Task 6 object write/read  *********************
    /**
     * Saves the state of the game to the file with the given name
     * 
     * @param filename the name of the file 
     */ 
    public void saveGame(String fname){
        try{
            /* Save simple version of this game to file. */
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fname, false));
            oos.writeObject(this);
            oos.close();
        }catch(FileNotFoundException e){}
        catch(IOException e){}
    }
    
    /**
     * Restores the game from the file with the given name
     * 
     * @param filename the name of the file 
     */
    public Game restoreGame(String fname){
        /* Set the game to null incase of game load failure. */
        Game game = null;
        try{
            /* Attempt to laof the game from file and cast it. If the game is
             * not of correct type it will not cast correctly. */
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fname));
            game = (Game)(ois.readObject());
            ois.close();
        }catch(ClassNotFoundException e){}
        catch(FileNotFoundException e){}
        catch(IOException e){}
        return game;
    } 
}
/* Addition: Changed the import java.io.* to java.io.Serializable so that
 * programmers are aware of any additional classes they use, and therefore the
 * potential overhead of doing so. This may have future impact on RAM but in
 * general is good practice to be aware of what is being imported into a
 * project. */
import java.io.Serializable;

/**
 * Enumeration class ChallengeType - write a description of the enum class here
 * 
 * @author Daniel Barry & Paul Moggridge
 * @version 1.1 21:10 03/03/2014 Prototype
 */
public enum ChallengeType implements Serializable{
    MAGIC("Magic"), FIGHT("Fight"), MYSTERY("Mystery");
    
    private String type;
    
    private ChallengeType(String ty){ type = ty; }
    
    /* The @Override annotation has also been added to tell future programmers
     * that overriding the toString() method was intentional. */
    @Override
    public String toString(){ return type; }
}

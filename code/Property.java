import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Properties;

/**
 * A class for safely loading properties files from disk and retrieving casted
 * values. As long as this class is used as intended, it has been designed not to
 * throw any excpetions, allowing the programmer using this class to be fairly
 * careless in it's use and not having to worry about config or properties files
 * exisiting, or values of their type. There is also error handling for
 * situations where a security manager has locked a file or the disk doesn't
 * return a file correctly.
 * 
 * @author Daniel Barry & Paul Moggridge
 * @version 1.0 17:50 23/02/2014 Prototype
 */
public class Property{
    /* Automatically setup as a faield to load Properties file to prevent issues
     * where the programmer hasn't specified a file to read from. */
    private Properties prop = null;
    
    /**
     * Allows a file to be loaded to be referenced. A boolean value is returned
     * to indicate the success of such, but underlying errors are hidden and dealt
     * with appropriately. If a file fails to load, a programmer using this method
     * does not strictly have to worry about having to check to see if loading
     * properties is okay. They can on the other hand attempt a different file or
     * change the circumstance under which the file failed to load.
     * 
     * @param name The name of the file to be loaded from disk. It is the
     * programmers responsibility to make sure the file name isn't operating
     * system dependant.
     * @return Indication of the success of a file load, where true indicates a
     * successful file load and false an unsuccessful file load.
     */
    public boolean loadFile(String name){
        /* Attempt to load values from a property file without the possibility of
         * crashing. Conversion is expected to be handled by the programmer using
         * this class. Please note that the caught exceptions are chained as
         * unchained excpetions were introduced in java 1.7. For compatibility
         * reasons the older version has been kept. A boolean value is returned to
         * allow a programmer to try to fix the issue of a file not loading
         * correctly. This may equally be ignored, in which case when loading a
         * value from file the condition that there may not be a correctly loaded
         * properties file also has to be considered. */
        try{
            /* Read from the file, where any bad reads cause the Properties to be
             * set to null where other classes will handle the issue. */
            prop = new Properties();
            prop.load(new FileInputStream(name));
            return true;
        }catch(FileNotFoundException e){
            /* On failure to load of the configuration file, set file to be prop to
             * be null. Return true to indicate file was loaded correctly. */
            prop = null;
            return false;
        }catch(IOException e){
            /* On failure to read from the input stream, set file to be prop to be
             * null. Return false to indicate file was loaded correctly. */
            prop = null;
            return false;
        }catch(SecurityException e){
            /* On failure load the file due to a security manager not allowing the
             * file to be read, set file to be prop to be null. Return false to
             * indicate file was loaded correctly. */
            prop = null;
            return false;
        }
    }
    
    /**
     * Offers the ability to load a casted value from the file referenced by name,
     * where a default value should be offered in the case of a failure and a Class
     * type should be given to cast both values. There is a pre-condition that the
     * default value if the correct type as specified by clazz. This is checked on
     * method call and will throw a Class Cast exception if defaultValue is not an
     * instance of the casting class.
     * <br />
     * Typically this method will be used to return Java primitives and Strings but
     * could potentially cast other objects in the same way. It is important to
     * consider that non-primtive data saved into a properties file will not be
     * easily editable, defeating the overall objective of having a properties file.
     * 
     * @param value The value to be looked up in the Properties file. This value
     * works the same way as found in the Properties getProperty method. Please
     * reference this for more information.
     * @param defaultValue This is the default value that will be returned if the
     * requested value fails to load for any reason. This value must also be
     * castable to the specified casting class.
     * @param clazz The class type for objects got in this method to be returned
     * as. The return value and default value should both be castable to this type.
     * @return A casted value containing either a value from file or the default
     * value. A null value should never be retunable from this method unless the
     * default value is null and casting type allows for a null value to be
     * returned.
     * @throws ClassCastException A RunTimeException thrown if default value is not
     * castable to the specified casting type. It is not reccomended that this is
     * caught under normal operation as this is an indication to this method being
     * used incorrectly.
     */
    public <T> T loadValue(String value, Object defaultValue, Class<T> clazz){
        /* Make sure that the default value is castable to the type. If the default
         * value isn't a correct instance of the Class passed, throw a class cast
         * exception to make programmer aware of mistake. */
        if(!clazz.isInstance(defaultValue))throw new ClassCastException("Default value is not an instance of the Class.");
        /* Check to for a null pointer in the location prop, therefore an unloaded
         * properties file. */
        if(prop!=null){
            /* Return the object as uncasted to simply check whether it points to
             * null or whether the object was correctly casted, at which point it's
             * reference is not null. */
            Object casted = castObject(prop.getProperty(value, null), clazz);
            if(casted!=null){
                /* If the object is not null and therefore correctly casted, recast
                 * the object and return it's casted value of correct type.*/
                return castObject(casted, clazz);
            }else{
                /* Return a cast of the defualt value. There should be no assumption
                 * that the Object passed is of the correct type. */
                return castObject(defaultValue, clazz);
            }
        }else{
            /* Return a cast of the defualt value. There should be no assumption
             * that the Object passed is of the correct type. */
            return castObject(defaultValue, clazz);
        }
    }
    
    /**
     * Allows an uncasted object to be casted to a given class type defined by
     * clazz. If the object can't be casted to type clazz a null pointer is
     * returned in it's place. This should be checked for.
     * 
     * @param obj The object to be casted to type clazz.
     * @param clazz The class specifying what the object should be casted as.
     * @return A casted object if obj was castable to type clazz, otherwise a null
     * pointer is returned indicating an error casting.
     */
    private static <T> T castObject(Object obj, Class<T> clazz){
        /* Attempt to cast the Object in question */
        try{
            /* Cast the object with the knowledge that it may not be castable. Return
             * a casted type object. */
            return clazz.cast(obj);
        }catch(ClassCastException e){
            /* If a cast fails, return a null value in it's place to indicate a failed
             * casting. */
            return null;
        }
    }
}
/**
 * An interface for a Player to implement giving the basic design structure for
 * any given game. This provides a separation between the "front end", which
 * handles interaction with the user, from the hidden "business end" which
 * performs game processing.
 * This interface has been designed to be reduced coupling which is why all
 * methods use Java primitives and Strings.
 * 
 * @author Daniel Barry
 * @version 1.0 01/02/2014
 */
public interface CODE{
    /**
     * Allows for a specified champion to be retrieved as a String.
     * 
     * @param name Is the name of the champion to be retrieved as a String
     * value.
     * @return A String containing details of the champion requested.
     */
    public String getChampion(String name);
    
    /**
     * This method returns a String containing details about all the champions
     * in the game. This is because the player may request to see a list of all
     * the champions.
     * 
     * @return A String containing the details of all of the champions.
     */
    public String getAllChampions();
    
    /**
     * A method that returns a String containing all of the champions that the
     * player has hired allowing them to view champions they own.
     * 
     * @return A string containing all of the player's champions details.
     */
    public String getPlayerChampions();
    
    /**
     * Returns a String containing all of the champions that the player has not
     * currently hired. This allows a player to view all champions available for
     * hire.
     * 
     * @return A String containing all of the champions the player has not hired
     * in the game.
     */
    public String getAvailableChampions();
    
    /**
     * A method hiring a champion by their unique name.
     * 
     * @param name The unique identifier champions name String value.
     * @return The result of trying to hire the requested champion.
     */
    public String hireChampion(String name);
    
    /**
     * This method allows for a champion that has been hired by a player to be
     * dismissed, adding half of the champions original cost back to the players
     * money.
     * 
     * @param name The unique identifier player's name String value.
     * @return A String message containing the result of dismissing a champion.
     */
    public String dismissChampion(String name);
    
    /**
     * A method to allow a champion to be restored from the resting state.
     * Please note that a resting champion cannot be used to meet any further
     * challenges until restored to the army on payment of 50 gulden.
     * 
     * @param name The champions unique identifier String name value to be
     * restored.
     * @return A suitable String message containing the result of restoring a
     * champion.
     */
    public String restoreChampion(String name);
    
    /**
     * This method returns the player's current amount of money. This is because
     * the player may request to see the state of their treasury.
     * 
     * @return A long containing the amount of money the player currently has.
     */
    public long getTreasure();
    
    /**
     * Allows the player to accept a challenge by the challenge number.
     * By accepting the challenge, the first appropriate champion will
     * be used to meet the challenge. The result of the challenge will be return
     * as a suitable String message to be displayed.
     * This method exists because this early version of the game allows the
     * player to select the challenge (by number) in order to make it possible
     * to design a standard set of tests. Programs with "random" features are
     * difficult to test because of their unpredictability.
     * 
     * @param challengeNum The challenge number that associates to a challenge
     * the player wants to attempt.
     * @return An appropriate String message containing the result of the
     * challenge, as specified below:
     * <ul>
     *   <li>“Challenge won by...“ – add reward to treasury, return name of
     * champion and set champion to "resting"</li>
     *   <li>“Challenge lost as no champion available” – deduct reward from
     * treasury</li>
     *   <li>“Challenge lost on skill level” - deduct reward from treasury and
     * set your champion to "dead"</li>
     * </ul>
     */
    public String acceptChallenge(int challengeNum);
    
    /**
     * Returns a challenge by it's challenge number as a player will need to
     * select a challenge by number (later versions will select challenges for
     * the player at random).
     * 
     * @param challengeNum An integer that references a particular challenge.
     * @return A String containing details of the specified challenge.
     */
    public String getChallenge(int challengeNum);
    
    /**
     * Retrieves all of the currently available challenges as a player may
     * request a list of all the possible challenges
     * 
     * @return A string containing all of the currently available challenges.
     */
    public String getAllChallenges();
    
    /**
     * Allow the ruler name to be retrieved.
     * 
     * @return A String containing the name of the ruler.
     */
    public String getRuler();
    
    /**
     * Allows for the game to be saved with a given filePath and the
     * result of the file save to be returned as boolean.
     * This is because on a request to terminate, the player's resources should
     * be displayed (in future, resources will be treated as points and posted
     * to a "High Scores" table). This method is responsible for the saving
     * section.
     * 
     * @param filePath A String with the location the file is to be saved to.
     * @return A boolean indicating the success of the save. True represents a
     * successful save and false represents an unsuccessful save.
     */
    public boolean saveGame(String filePath);
    
    /**
     * Allows a player to load their previously saved game with a
     * given filePath, where the result of the game load is returned as a
     * boolean.
     * On player creation the system will create an instance of the game and
     * automatically load all the required data on champions and challenges into
     * appropriate variables/collections and set the treasury to 1000 gulden.
     * 
     * @param filePath A String with the location of the game save to be loaded.
     * @return A boolean indicating the success of the game load. True
     * represents a successful game load and false represents an unsuccessful
     * load.
     */
    public boolean loadGame(String filePath);
    
    /**
     * Get a String containing details about the current game, including the
     * state of their treasury, all the champions in their army (and their
     * states), all champions available for hire, a list of all the possible
     * challenges, the state of one of the champions in their army.
     * 
     * @return String containing the details of the player's current game.
     */
    public String getPlayerDeatils();
    
    /**
     * Allow the player to terminate the game at any point. On a request to
     * terminate, the player's resources should be displayed (in future,
     * resources will be treated as points and posted to a "High Scores" table).
     */
    public void terminateGame();
}